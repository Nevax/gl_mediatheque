package fr.ui.gl.mediatheque;

import java.sql.Date;
import java.time.LocalDateTime;

import fr.core.gl.mediatheque.Auteur;
import fr.core.gl.mediatheque.Ouvrage;
import fr.core.gl.mediatheque.Editeur;
import fr.core.gl.mediatheque.Mediatheque;
import fr.core.gl.mediatheque.Pret;
import fr.core.gl.mediatheque.Relance;
import fr.core.gl.mediatheque.Usager;
import fr.dao.gl.mediatheque.AbstractDAOFactory;
import fr.dao.gl.mediatheque.DAO;
import fr.dao.gl.mediatheque.DAOSettings;
import fr.dao.gl.mediatheque.FactoryType;

/**
 * Class menu servant de class maîtresse ainsi qu'à l'affiche de l'IHM après de
 * l'utilisateur. Celle-ci contient aussi le bouchon servant à l'initialisation
 * des données permettant de tester le bon fonctionnement du programme.
 * 
 * @author Antoine GRAVELOT
 * @version 1.0
 */
public class Menu extends Mediatheque {

	private static Usager		usager;
	private static Ouvrage		ouvrage;
	private static Pret			pret;

	/**
	 * DAO d'Usager en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Usager
	 */
	private static DAO<Usager>	usagerdao;
	/**
	 * DAO de Ouvrage en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Ouvrage
	 */
	private static DAO<Ouvrage>	ouvragedao;
	/**
	 * DAO de l'Auteur en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Auteur
	 */
	private static DAO<Auteur>	auteurdao;
	/**
	 * DAO de Pret en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Pret
	 */
	private static DAO<Pret>	pretdao;
	/**
	 * DAO de Relance en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Relance
	 */
	private static DAO<Relance>	relancedao;
	/**
	 * DAO de l'Editeur en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Editeur
	 */
	private static DAO<Editeur>	editeurdao;

	/**
	 * Méthode du menu principal
	 * 
	 * @param args
	 *            Paramètre non utilsé
	 */
	public static void main(String[] args) {

		/*
		 * Instancie les différents DAO en fonction de la configuration donné
		 * dans le fichier dee configuration. Si un DAO spécifique donné,
		 * l'utiliser pour l'ensemble des DAO. Sinon utiliser ces DAO:
		 */
		// - usagerdao = SQL_DAO_FACTORY
		// - ouvragedao = XML_DAO_Factory
		// - auteurdao = PARSED_DAO_Factory
		// - pretdao = SQL_DAO_FACTORY
		// - relancedao = SQL_DAO_FACTORY
		// - editeurdao = EDI_DAO_Factory

		if (DAOSettings.getDAOSettings() == FactoryType.ALL_DAO_Factory) {
			usagerdao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getUsagerDAO();
			ouvragedao = AbstractDAOFactory.getFactory(FactoryType.XML_DAO_Factory).getOuvrageDAO();
			auteurdao = AbstractDAOFactory.getFactory(FactoryType.PARSED_DAO_Factory).getAuteurDAO();
			pretdao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getPretDAO();
			relancedao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getRelanceDAO();
			editeurdao = AbstractDAOFactory.getFactory(FactoryType.EDI_DAO_Factory).getEditeurDAO();
		} else {
			usagerdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getUsagerDAO();
			ouvragedao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getOuvrageDAO();
			auteurdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getAuteurDAO();
			pretdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getPretDAO();
			relancedao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getRelanceDAO();
			editeurdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getEditeurDAO();
		}

		int kb_input;
		boolean reroll = true;

		/**
		 * Menu principale qui se relance tant que l'utilisateur ne quitte pas
		 * le logiciel.
		 */
		while (reroll) {
			// Affiche le menu
			System.out.println("============================");
			System.out.println("|   GESTION BIBLIOTHEQUE   |");
			System.out.println("============================");
			System.out.println("|       1. Usager          |");
			System.out.println("|       2. Ouvrages        |");
			System.out.println("|       3. Editeur         |");
			System.out.println("|       4. Auteurs         |");
			System.out.println("|       5. Prêts           |");
			System.out.println("|       6. Relance         |");
			System.out.println("|       0. Quitter         |");
			System.out.println("============================");

			kb_input = Keyin.inInt("Sélectionnez une option: ");

			/** Switch pour entrer dans les sous menus */
			switch (kb_input) {
			case 1:
				menuUsager();
				break;
			case 2:
				menuOuvrages();
				break;
			case 3:
				menuEditeurs();
				break;
			case 4:
				menuAuteurs();
				break;
			case 5:
				menuPrets();
				break;
			case 6:
				menuRelance();
				break;
			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entrée invalide");
				break;
			}
		}
		System.out.println("Au revoir!");
	}

	/**
	 * Affiche tout les usagers et demande à l'utilisateur d'en sélectionner un.
	 * 
	 * @return i
	 */
	public static int selectUsager() {
		int kb_input;

		System.out.println("Liste des usagers");
		System.out.println("ID  Nom    Prénom");
		System.out.println(getUsagerHolderAll());
		for (int i = 0; i < getUsagerHolderAll().size(); i++) {
			// TODO ID -1 ??
			System.out.println(getUsagerHolderAll().get(i).getId() + getUsagerHolderAll().get(i).getCivilite() + " "
					+ getUsagerHolderAll().get(i).getNom() + " " + getUsagerHolderAll().get(i).getPrenom());
		}

		kb_input = Keyin.inInt("Sélectionnez un identifiant : ");

		for (int i = 0; i < getUsagerHolderAll().size(); i++) {
			// System.out.println(i);
			if (getUsagerHolderAll().get(i).getId() == kb_input) {
				return i;
			}
		}
		return -1;
	}

	public static int selectOuvrage() {
		int kb_input;

		System.out.println("Liste des Ouvrages");
		System.out.println("Code  Titre    Pretable   Disponibilité");
		for (int i = 0; i < getOuvrageHolderAll().size(); i++) {
			// TODO ID -1 ??
			System.out.println(getOuvrageHolderAll().get(i).getCode() + " " + getOuvrageHolderAll().get(i).getTitre()
					+ " " + getOuvrageHolderAll().get(i).getPretable() + " "
					+ getOuvrageHolderAll().get(i).getDisponible() + " " + getOuvrageHolderAll().get(i).getDateAchat());
		}

		kb_input = Keyin.inInt("Sélectionnez un identifiant : ");

		for (int i = 0; i < getOuvrageHolderAll().size(); i++) {
			// System.out.println(i);
			if (getOuvrageHolderAll().get(i).getCode() == kb_input) {
				return i;
			}
		}
		return -1;
	}

	public static int selectPret() {
		int kb_input;

		System.out.println("Liste des prets");
		System.out.println("ID  ID_Usager ID_Document DateDeSortie DateDeRetour Retouné?");
		for (int i = 0; i < getPretHolderAll().size(); i++) {
			// TODO ID -1 ??
			System.out.println(getPretHolderAll().get(i).getId() + " " + getPretHolderAll().get(i).getDateSortie() + " "
					+ getPretHolderAll().get(i).getDateRetour() + " " + getPretHolderAll().get(i).getRetour());
		}

		kb_input = Keyin.inInt("Sélectionnez un identifiant : ");

		for (int i = 0; i < getPretHolderAll().size(); i++) {
			// System.out.println(i);
			if (getPretHolderAll().get(i).getId() == kb_input) {
				return i;
			}
		}
		return -1;
	}

	public static void menuPrets() {

		boolean reroll = true;
		int id;

		while (reroll) {

			System.out.println("======================================");
			System.out.println("|           GESTION PRETS            |");
			System.out.println("======================================");
			System.out.println("|       1. Détail du prêt            |");
			System.out.println("|       2. Ajouter Prêt              |");
			System.out.println("|       3. Supprimer prêt            |");
			System.out.println("|       4. Modifier prêt             |");
			System.out.println("|       0. Retour                    |");
			System.out.println("======================================");

			int kb_input;
			kb_input = Keyin.inInt("Sélectionnez une option: ");

			// Switch construct
			switch (kb_input) {
			case 1:
				id = selectPret();
				// Si un utilisateur est trouvé pour l'id saisi par
				// l'utilisateur
				// Sinon retourn au menu principale
				if (id != -1) {
					// Détail usager
					System.out.println("Identifiant : " + getPretHolderAll().get(id).getId());
					System.out.println("Usager : " + getPretHolderAll().get(id).getUsager());
					System.out.println("Ouvrage : " + getPretHolderAll().get(id).getOuvrage());
					System.out.println("Date sortie : " + getPretHolderAll().get(id).getDateSortie());
					System.out.println("Date retour : " + getPretHolderAll().get(id).getDateRetour());
					System.out.println("Retour : " + getPretHolderAll().get(id).getRetour());
				} else {
					System.out.println("Aucun utilisateur trouvé pour lidentifiant saisi");
					return;
				}
				break;
			case 2:

				pretdao.create(new Pret(10, usager = new Usager(5), ouvrage = new Ouvrage(5), LocalDateTime.now(),
						LocalDateTime.now(), true));
				break;
			case 3:
				// Demande l'identifiant de la ligne à supprimer
				Integer idToDel = Keyin.inInt("Entrez l'identifiant de l'usager à supprimer (0 pour annuler) : ");
				// Si l'utilisateur à entré 0 ou vide
				if (idToDel != 0) {
					pretdao.delete(new Pret(idToDel));
				}
				break;
			case 4:
				// Demande l'identifiant de la ligne à supprimer
				Integer selectID = Keyin.inInt("Entrez l'identifiant de l'usager à supprimer (0 pour annuler) : ");
				// Si l'utilisateur à entré 0 ou vide
				if (selectID != 0) {
					pretdao.update(new Pret(selectID, usager = new Usager(10), ouvrage = new Ouvrage(10),
							LocalDateTime.now(), LocalDateTime.now(), true));
				}
				break;
			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entré invalide");
				break;
			}
		}
	}

	public static int selectEditeur() {
		int kb_input;

		System.out.println("Liste des éditeurs");
		System.out.println("ID  Nom");
		for (int i = 0; i < getEditeurHolderAll().size(); i++) {
			// TODO ID -1 ??
			System.out.println(getEditeurHolderAll().get(i).getId() + " " + getEditeurHolderAll().get(i).getNom());
		}

		kb_input = Keyin.inInt("Sélectionnez un identifiant : ");

		for (int i = 0; i < getEditeurHolderAll().size(); i++) {
			// System.out.println(i);
			if (getEditeurHolderAll().get(i).getId() == kb_input) {
				return i;
			}
		}
		return -1;
	}

	public static void menuEditeurs() {

		boolean reroll = true;
		int id = selectEditeur();

		while (reroll) {
			// Si un utilisateur est trouvé pour l'id saisi par l'utilisateur
			// Sinon retourn au menu principale
			if (id != -1) {
				System.out.println("======================================");
				System.out.println("|           GESTION EDITEURS         |");
				System.out.println("======================================");
				System.out.println("|       1. Détail éditeur              |");
				System.out.println("|       2.           |");
				System.out.println("|       3.   |");
				System.out.println("|       0. Retour                    |");
				System.out.println("======================================");
				System.out.println("Usager sélectionné : " + getEditeurHolderAll().get(id).getId() + " "
						+ getEditeurHolderAll().get(id).getNom());
			} else {
				System.out.println("Aucun utilisateur trouvé pour lidentifiant saisi");
				return;
			}

			int kb_input;
			kb_input = Keyin.inInt("Sélectionnez une option: ");

			// Switch construct
			switch (kb_input) {
			case 1:
				// Détail usager
				System.out.println("Identifiant : " + getEditeurHolderAll().get(id).getId());
				System.out.println("Nom : " + getEditeurHolderAll().get(id).getNom());
				break;
			case 2:

				break;
			case 3:

				break;
			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entré invalide");
				break;
			}
		}
	}

	public static int selectRelance() {
		int kb_input;

		System.out.println("Liste des relances");
		System.out.println("ID  Nom    Prénom");
		for (int i = 0; i < getRelanceHolderAll().size(); i++) {
			// TODO ID -1 ??
			System.out.println(getRelanceHolderAll().get(i).getId() + " " + getRelanceHolderAll().get(i).getPret() + " "
					+ getRelanceHolderAll().get(i).getDate() + " " + getRelanceHolderAll().get(i).getLibCourrier() + " "
					+ getRelanceHolderAll().get(i).getCodeEtat() + " " + getRelanceHolderAll().get(i).getCodeMode());
		}

		kb_input = Keyin.inInt("Sélectionnez un identifiant : ");

		for (int i = 0; i < getRelanceHolderAll().size(); i++) {
			// System.out.println(i);
			if (getRelanceHolderAll().get(i).getId() == kb_input) {
				return i;
			}
		}
		return -1;
	}

	public static void menuRelance() {
		boolean reroll = true;
		int id;

		while (reroll) {

			System.out.println("======================================");
			System.out.println("|           GESTION RELANCE          |");
			System.out.println("======================================");
			System.out.println("|       1. Détail relance              |");
			System.out.println("|       2. Ajouter relance           |");
			System.out.println("|       3. Supprimer reelance        |");
			System.out.println("|       4. Modifier relance          |");
			System.out.println("|       0. Retour                    |");
			System.out.println("======================================");

			int kb_input;
			kb_input = Keyin.inInt("Sélectionnez une option: ");

			// Switch construct
			switch (kb_input) {
			case 1:
				id = selectRelance();
				// Si un utilisateur est trouvé pour l'id saisi par
				// l'utilisateur
				// Sinon retourn au menu principale
				if (id != -1) {
					// Détail usager
					System.out.println("Identifiant : " + getRelanceHolderAll().get(id).getId());
					System.out.println("Usager : " + getRelanceHolderAll().get(id).getPret());
					System.out.println("Ouvrage : " + getRelanceHolderAll().get(id).getDate());
					System.out.println("Date sortie : " + getRelanceHolderAll().get(id).getLibCourrier());
					System.out.println("Date retour : " + getRelanceHolderAll().get(id).getCodeEtat());
					System.out.println("Retour : " + getRelanceHolderAll().get(id).getCodeMode());
				} else {
					System.out.println("Aucun utilisateur trouvé pour lidentifiant saisi");
					return;
				}
				break;
			case 2:
				relancedao.create(new Relance(1, pret = new Pret(5), LocalDateTime.now(), "azeaze", 1, "azeazeaze"));
				break;
			case 3:
				// Demande l'identifiant de la ligne à supprimer
				Integer idToDel = Keyin.inInt("Entrez l'identifiant de la relance à supprimer (0 pour annuler) : ");
				// Si l'utilisateur à entré 0 ou vide
				if (idToDel != 0) {
					relancedao.delete(new Relance(idToDel));
				}
				break;
			case 4:
				// Demande l'identifiant de la ligne à supprimer
				Integer selectID = Keyin.inInt("Entrez l'identifiant la relance à supprimer (0 pour annuler) : ");
				// Si l'utilisateur à entré 0 ou vide
				if (selectID != 0) {
					relancedao.update(new Relance(selectID, pret = new Pret(5), LocalDateTime.now(), "azeaze", 1, "azeazeaze"));
				}
				break;

			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entré invalide");
				break;
			}
		}
	}

	public static int selectAuteur() {
		int kb_input;

		System.out.println("Liste des auteurs");
		System.out.println("ID  Nom    Prénom");
		for (int i = 0; i < getAuteurHolderAll().size(); i++) {
			System.out.println(getAuteurHolderAll().get(i).getId() + " " + getAuteurHolderAll().get(i).getNom() + " "
					+ getAuteurHolderAll().get(i).getPrenom());
		}
		kb_input = Keyin.inInt("Sélectionnez un identifiant : ");
		for (int i = 0; i < getAuteurHolderAll().size(); i++) {
			// System.out.println(i);
			if (getAuteurHolderAll().get(i).getId() == kb_input) {
				return i;
			}
		}
		return -1;
	}

	public static void menuAuteurs() {

		boolean reroll = true;
		int id = selectAuteur();

		while (reroll) {
			// Si un utilisateur est trouvé pour l'id saisi par l'utilisateur
			// Sinon retourn au menu principale
			if (id != -1) {
				System.out.println("======================================");
				System.out.println("|           GESTION AUTEURS           |");
				System.out.println("======================================");
				System.out.println("|       1. Détail auteurs            |");
				System.out.println("|       2.            |");
				System.out.println("|       3.   |");
				System.out.println("|       0. Retour                    |");
				System.out.println("======================================");
				System.out.println("Usager sélectionné : " + getAuteurHolderAll().get(id).getId() + " "
						+ getAuteurHolderAll().get(id).getNom() + " " + getAuteurHolderAll().get(id).getPrenom());
			} else {
				System.out.println("Aucun utilisateur trouvé pour lidentifiant saisi");
				return;
			}

			int kb_input;
			kb_input = Keyin.inInt("Sélectionnez une option: ");

			// Switch construct
			switch (kb_input) {
			case 1:
				// Détail usager
				System.out.println("Identifiant : " + getAuteurHolderAll().get(id).getId());
				System.out.println("Usager : " + getAuteurHolderAll().get(id).getNom());
				System.out.println("Ouvrage : " + getAuteurHolderAll().get(id).getPrenom());
				break;
			case 2:

				break;
			case 3:

				break;
			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entré invalide");
				break;
			}
		}
	}

	/* ---------- USAGER ------------------ */
	/**
	 * Affiche le meun usager permettant à l'utilisateur de manipuler les
	 * usagers.
	 */
	public static void menuUsager() {

		boolean reroll = true;
		int id;

		while (reroll) {

			System.out.println("======================================");
			System.out.println("|           GESTION USAGER           |");
			System.out.println("======================================");
			System.out.println("|       1. Détail usager             |");
			System.out.println("|       2. Ajouter usager            |");
			System.out.println("|       3. Supprimier usager         |");
			System.out.println("|       4. Modifier usager           |");
			System.out.println("|       0. Retour                    |");
			System.out.println("======================================");

			int kb_input;
			kb_input = Keyin.inInt("Sélectionnez une option: ");

			// Switch construct
			switch (kb_input) {
			case 1:
				id = selectUsager();
				// Si un utilisateur est trouvé pour l'id saisi par
				// l'utilisateur
				// Sinon retourn au menu precédent
				if (id != -1) {
					// Détail usager
					System.out.println("Identifiant : " + getUsagerHolderAll().get(id).getId());
					System.out.println("Civilite : " + getUsagerHolderAll().get(id).getCivilite());
					System.out.println("Nom : " + getUsagerHolderAll().get(id).getNom());
					System.out.println("Prenom : " + getUsagerHolderAll().get(id).getPrenom());
					System.out.println("Adresse postale : " + getUsagerHolderAll().get(id).getAdressePostale());
					System.out.println("Date inscription : " + getUsagerHolderAll().get(id).getDateInscription());
					System.out.println("Montant cotisation : " + getUsagerHolderAll().get(id).getCotisation());
					System.out.println("Nombre de prêt : " + getUsagerHolderAll().get(id).getNombrePret());
					System.out.println("Identifiant : " + getUsagerHolderAll().get(id).getAdresseEmail());
				} else {
					System.out.println("Aucun utilisateur trouvé pour lidentifiant saisi");
					return;
				}
				break;
			case 2:
				usagerdao.create(
						new Usager(50, 0, "azeAAAAAAAAaa", "aze", "aze", new Date(32132132), 1 / 10000, 1, "azeaze"));
				break;
			case 3:
				// Demande l'identifiant de la ligne à supprimer
				Integer idToDel = Keyin.inInt("Entrez l'identifiant de l'usager à supprimer (0 pour annuler) : ");
				// Si l'utilisateur à entré 0 ou vide
				if (idToDel != 0) {
					usagerdao.delete(new Usager(idToDel));
				}
				break;
			case 4:
				// Demande l'identifiant de la ligne à supprimer
				Integer selectID = Keyin.inInt("Entrez l'identifiant de l'usager à supprimer (0 pour annuler) : ");
				// Si l'utilisateur à entré 0 ou vide
				if (selectID != 0) {
					usagerdao.update(new Usager(selectID, 0, "BBBBBBBBBBBB", "aze", "aze", new Date(32132132),
							1 / 10000, 1, "azeaze"));
				}
				break;
			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entré invalide");
				break;
			}
		}
	}

	/* ----------- Ouvrage ------------ */
	/**
	 * Menu Ouvrage permettant la gestion des Ouvrages.
	 */
	public static void menuOuvrages() {
		int kb_input;
		boolean reroll = true;
		int id = selectOuvrage();
		while (reroll) {

			if (id != -1) {
				System.out.println("======================================");
				System.out.println("|           GESTION OuvrageS         |");
				System.out.println("======================================");
				System.out.println("|       1. Détail Ouvrage            |");
				System.out.println("|       2.              |");
				System.out.println("|       3.   |");
				System.out.println("|       0. Retour                    |");
				System.out.println("======================================");

				kb_input = Keyin.inInt("Sélectionnez une option: ");
			} else {
				System.out.println("Aucun Ouvrage trouvé pour lidentifiant saisi");
				return;
			}

			// Switch construct
			switch (kb_input) {
			case 1:
				System.out.println("Identifiant : " + getOuvrageHolderAll().get(id).getCode());
				System.out.println("Auteur : " + getOuvrageHolderAll().get(id).getAuteur());
				System.out.println("Editeur : " + getOuvrageHolderAll().get(id).getEditeur());
				System.out.println("Titre : " + getOuvrageHolderAll().get(id).getTitre());
				System.out.println("Pretable : " + getOuvrageHolderAll().get(id).getPretable());
				System.out.println("Disponibilité : " + getOuvrageHolderAll().get(id).getDisponible());
				System.out.println("Date acaht : " + getOuvrageHolderAll().get(id).getDateAchat());
				break;
			case 2:

				break;
			case 3:

				break;
			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entré invalide");
				break;
			}
		}
	}

	/* ----------- RETARDS ------------- */
	/**
	 * Menu retard permettant la gestion des retards
	 */
	public static void menuRetards() {
		boolean reroll = true;
		while (reroll) {
			System.out.println("1. ");
			System.out.println("2. ");
			System.out.println("3. ");
			System.out.println("0. Retour");

			int kb_input;
			kb_input = Keyin.inInt("Sélectionnez une option: ");

			// Switch construct
			switch (kb_input) {
			case 1:
				menuUsager();
				break;
			case 2:
				menuOuvrages();
				break;
			case 3:
				menuRetards();
				break;
			case 0:
				reroll = false;
				break;
			default:
				System.out.println("Entré invalide");
				break; // This break is not really necessary
			}
		}
	}

}

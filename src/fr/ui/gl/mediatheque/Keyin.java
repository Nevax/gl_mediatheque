package fr.ui.gl.mediatheque;

/**
 * Class servant à vérification des saisi réalisé par l'utilsiateur. Incluant u
 * ne gestion des erreurs.
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
class Keyin {

	/**
	 * Verifie que la saisi utilisateur n'est pas vide.
	 */
	public static void inputFlush() {
		int dummy;
		int bAvail;

		try {
			while ((System.in.available()) != 0)
				dummy = System.in.read();
		} catch (java.io.IOException e) {
			System.out.println("Input error");
		}
	}

	/**
	 * Affiche un message à l'utilisateur, demande la saisi d'une chaine de
	 * caractère, vérifie que celui-ci en est bien une. Retourne la chaine de
	 * caractère si convertissable en String, sinon affiche une erreur.
	 * 
	 * @param prompt
	 * @return s
	 */
	public static String inString(String prompt) {
		inputFlush();
		System.out.println(prompt);
		return inString();
	}

	/**
	 * Demande la saisi d'une chaine de caractère, vérifie que celui-ci en est
	 * bien une. Retourne la chaine de caractère si convertissable en String,
	 * sinon affiche une erreur.
	 * 
	 * @param prompt
	 * @return s
	 */
	public static String inString() {
		int aChar;
		String s = "";
		boolean finished = false;

		while (!finished) {
			try {
				aChar = System.in.read();
				if (aChar < 0 || (char) aChar == '\n')
					finished = true;
				else if ((char) aChar != '\r')
					s = s + (char) aChar; // Enter into string
			}

			catch (java.io.IOException e) {
				System.out.println("Erreur, entrée invalide");
				finished = true;
			}
		}
		return s;
	}

	/**
	 * Affiche un message à l'utilisateur, demande la saisi d'un Integer,
	 * vérifie que celui-ci en est bien un. Retourne l'Integer si convertissable
	 * en Integer, sinon affiche une erreur.
	 * 
	 * @param prompt
	 * @return Integer.valueOf(inString().trim()).intValue();
	 */
	public static int inInt(String prompt) {
		while (true) {
			inputFlush();
			System.out.println(prompt);
			try {
				return Integer.valueOf(inString().trim()).intValue();
			}

			catch (NumberFormatException e) {
				System.out.println("Entrée invalide. N'est pas un entier");
			}
		}
	}

	/**
	 * Affiche un message à l'utilisateur, demande la saisi d'un char, vérifie
	 * que celui-ci en est bien un. Retourne le cha si convertissable en char,
	 * sinon affiche une erreur.
	 * 
	 * @param prompt
	 * @return (char) aChar;
	 */
	public static char inChar(String prompt) {
		int aChar = 0;

		inputFlush();
		System.out.println(prompt);

		try {
			aChar = System.in.read();
		}

		catch (java.io.IOException e) {
			System.out.println("Erreur, entrée invalide");
		}
		inputFlush();
		return (char) aChar;
	}

	/**
	 * Affiche un message à l'utilisateur, demande la saisi d'un double, vérifie
	 * que celui-ci en est bien un. Retourne le double si convertissable en
	 * double, sinon affiche une erreur.
	 * 
	 * @param prompt
	 * @return Double.valueOf(inString().trim()).doubleValue();
	 */
	public static double inDouble(String prompt) {
		while (true) {
			inputFlush();
			System.out.println(prompt);
			try {
				return Double.valueOf(inString().trim()).doubleValue();
			}

			catch (NumberFormatException e) {
				System.out.println("Entrée invalide, n'est pas un nombre à virgule.");
			}
		}
	}
}

package fr.dao.gl.mediatheque;

import java.sql.ResultSet;
import java.sql.SQLException;

import fr.core.gl.mediatheque.Editeur;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class SQLEditeurDAO extends DAO<Editeur> {

	private static String QUERY_FIND_EDITEUR = "SELECT * FROM `EDITEUR`";

	public SQLEditeurDAO() {
		this.read();
	}

	@Override
	public void read() {
		try {
			statement = ConnectionSQL.getInstance().createStatement();
			ResultSet rset = statement.executeQuery(QUERY_FIND_EDITEUR);

			while (rset.next()) {
				Integer id = rset.getInt("ID_EDITEUR");
				String nom = rset.getString("NOM_EDITEUR");
				editeurHolderAll.add(new Editeur(id, nom));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Editeur find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Editeur create(Editeur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Editeur update(Editeur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Editeur obj) {
		// TODO Auto-generated method stub

	}

}

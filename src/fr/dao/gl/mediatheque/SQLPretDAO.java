package fr.dao.gl.mediatheque;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.junit.validator.PublicClassValidator;

import fr.core.gl.mediatheque.Editeur;
import fr.core.gl.mediatheque.Ouvrage;
import fr.core.gl.mediatheque.Pret;
import fr.core.gl.mediatheque.Usager;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class SQLPretDAO extends DAO<Pret> {

	private static String QUERY_FIND_PRET = "SELECT * FROM `PRET`";

	public SQLPretDAO() {
		this.read();
	}

	@Override
	public void read() {
		try {
			statement = ConnectionSQL.getInstance().createStatement();
			ResultSet rset = statement.executeQuery(QUERY_FIND_PRET);

			while (rset.next()) {
				Integer id = rset.getInt("ID_PRET");
				Integer idUsager = rset.getInt("ID_USAGER");
				// TODO Trouver l'usager à partir de l'identifiant
				Usager usager = findUsagerById(idUsager);
				
				Integer idOuvrage = rset.getInt("CODE_DOCUMENT");
				// TODO Trouver l'ouvrage à partir de l'identifiant
				Ouvrage ouvrage = findOuvragBeyId(idOuvrage);
				LocalDateTime dateSortie = LocalDateTime.now();
				// LocalDateTime dateSorie = rset.getDate("DATE_SORTIE_PRET");
				LocalDateTime dateRetour = LocalDateTime.now();
				// LocalDateTime dateRetour = rset.getDate("DATE_RETOUR_PRET");
				Boolean retour = rset.getBoolean("RETOUR_PRET");
				// LocalDateTime dateAchat =
				// rset.getDate("DATE_ACHAT_Ouvrage");
				if (usager != null || ouvrage != null) {
					pretHolderAll.add(new Pret(id, usager, ouvrage, dateSortie, dateRetour, retour));
					 System.out.println(id + " " + 
					usager.getId() + " " + 
							 ouvrage.getCode()
					 + " " + dateSortie + " " + dateRetour + " "
					 + retour );
				}
				
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	private Usager findUsagerById(int id) {
		for (Usager usager : getUsagerHolderAll()) {
			if (usager.getId() == id) {
				return usager;
			}
		}
		return null;
	}
	
	private Ouvrage findOuvragBeyId(int id) {
		for (Ouvrage ouvrage : getOuvrageHolderAll()) {
			if (ouvrage.getCode() == id) {
				return ouvrage;
			}
		}
		return null;
	}

	@Override
	public Pret find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Pret create(Pret obj) {
		try {
			// Préparation de la commande à partir de PreparedStatement,
			// l'identifiant est gérer par la DB
			PreparedStatement prepare = this.connection.prepareStatement(
					"INSERT INTO `PRET` (`ID_USAGER`, `CODE_DOCUMENT`, `DATE_SORTIE_PRET`, `DATE_RETOUR_PRET`, "
							+ "`RETOUR_PRET`) " + "VALUES ( ?, ?, ?, ?, ?)");

			// Ajout des arguments
			prepare.setInt(1, obj.getUsager().getId());
			prepare.setInt(2, obj.getOuvrage().getCode());
			prepare.setDate(3, null);
			prepare.setDate(4, null);
			prepare.setBoolean(5, obj.getRetour());

			// Execution de la commande
			prepare.executeUpdate();
			System.out.println("Prêt ajouté !");
		} catch (SQLException e) {
			System.out.println("Impossible d'ajouter l'usager id : " + obj.getId() + " à la base dee données.");
			e.printStackTrace();
			return null;
		}
		return obj;
	}

	@Override
	public Pret update(Pret obj) {
		try {
			PreparedStatement prepare = this.connection.prepareStatement(
					"UPDATE `PRET` SET " + " ID_USAGER = ?," + " CODE_DOCUMENT = ?" + ", DATE_SORTIE_PRET = ?"
							+ ", DATE_RETOUR_PRET = ?" + ", RETOUR_PRET = ?" + " WHERE `ID_PRET` = ?");

			prepare.setInt(1, obj.getUsager().getId());
			prepare.setInt(2, obj.getOuvrage().getCode());
			prepare.setDate(3, null);
			prepare.setDate(4, null);
			prepare.setBoolean(5, obj.getRetour());
			prepare.setInt(6, obj.getId());


			// Execution de la commande
			prepare.executeUpdate();
			System.out.println("Pret " + obj.getId() + " modifier !");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

	@Override
	public void delete(Pret obj) {
		try {
			statement.executeQuery("DELETE FROM PRET WHERE ID_PRET = " + obj.getId());
			// usagerHolderAll.remove(obj);
			// setUsagerHolderAll(getUsagerHolderAll().remove(obj));
			System.out.println("Prêt N°" + obj.getId() + " supprimé");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

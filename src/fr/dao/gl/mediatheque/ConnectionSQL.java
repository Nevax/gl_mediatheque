
package fr.dao.gl.mediatheque;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import fr.core.gl.mediatheque.Mediatheque;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 *
 */
public abstract class ConnectionSQL extends Mediatheque {
	/**
	 * Driver du JDBC
	 */
	private static String		db_driver;
	/**
	 * URL de la connection au JDBC
	 */
	private static String		db_url;
	/**
	 * Login de la connection au JDBC
	 */
	private static String		db_login;
	/**
	 * Mot de passe du login de la connection au JDBC
	 */
	private static String		db_pasword;
	/**
	 * Objet Conecction
	 */
	public static Connection	connection;
	
	/**
	 * Status de la connection
	 * false Réussi
	 * true non connecté/échec
	 */
	private static Boolean connectionStatus = false;

	/**
	 * Retourne notre instance Connection et la créer si elle n'éxiste pas (en
	 * appellant createConnection())
	 * 
	 * @return connection Instance connecté au JDBC
	 */
	public static Connection getInstance() {
		// Si pas de connection en créé une.
		if (connection == null) {
			connection = createConnection();
		}
		return connection;
	}

	/**
	 * Initialise les configurations à partir du fichier de configuration. Puis
	 * instancie une instance Connection connecté au JDBC, puis la retourne
	 * 
	 * @return connection Instance connecté au JDBC
	 * @see DAOSettings
	 */
	public static Connection createConnection() {
		try {
			// Initialise les configuration de la class ConnectionSQL
			DAOSettings.readSQLSettings();
			System.out.println("--------------------------");
			System.out.println("DRIVER: " + db_driver);
			// charge le drriver
			Class.forName(db_driver);
			// Créé la connection
			connection = DriverManager.getConnection(db_url, db_login, db_pasword);
			System.out.println("CONNECTION: " + connection);
			// récupère les métadatas
			DatabaseMetaData metaData = connection.getMetaData();
			// Affiche quelques informations sur le JDBC
			System.out.println(metaData.getDatabaseProductName());
			System.out.println(metaData.getDatabaseProductVersion());
			return connection;
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Connection à la base de donnés impossible");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Exécute une commande SQL auprès du JDBC
	 * 
	 * @param cmd
	 *            Commande SQL
	 */
	public void runSqlStatement(String cmd) {
		try {
			Statement statement = getInstance().createStatement();
			statement.execute(cmd);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Getter du driver utilisé pour la connection auprès du JDBC
	 * 
	 * @return db_driver Driver SQL
	 */
	public static String getDb_driver() {
		return db_driver;
	}

	/**
	 * Setter du driver utilisé pour la connection auprès du JDBC
	 * 
	 * @param db_driver
	 *            Driver SQL
	 */
	public static void setDb_driver(String db_driver) {
		ConnectionSQL.db_driver = db_driver;
	}

	/**
	 * Getter de l'URL de connection au JDBC
	 * 
	 * @return db_url URL du JDBC
	 */
	public static String getDb_url() {
		return db_url;
	}

	/**
	 * Setter de l'URL de connection au JDBC
	 * 
	 * @param db_url
	 *            URL du JDBC
	 */
	public static void setDb_url(String db_url) {
		ConnectionSQL.db_url = db_url;
	}

	/**
	 * Getter du login au JDBC
	 * 
	 * @return db_login Login du JDBC
	 */
	public static String getDb_login() {
		return db_login;
	}

	/**
	 * Setter du login au JDBC
	 * 
	 * @param db_login
	 *            Login du JDBC
	 */
	public static void setDb_login(String db_login) {
		System.out.println("ConnectionSQL: Set db login from "+ ConnectionSQL.db_login + " to > " + db_login);
		ConnectionSQL.db_login = db_login;
	}

	/**
	 * Getter du mot de passe au JDBC
	 * 
	 * @return db_pasword Mot de passe
	 */
	public static String getDb_pasword() {
		return db_pasword;
	}

	/**
	 * Setter du mot de passe au JDBC
	 * 
	 * @param db_pasword
	 *            Mot de passe
	 */
	public static void setDb_pasword(String db_pasword) {
		System.out.println("Setting SQL password from " + ConnectionSQL.db_pasword + " to " + db_pasword);
		ConnectionSQL.db_pasword = db_pasword;
	}
	
	/**
	 * Getter du statut de la connection
	 * @return connectionStatus Status de la connection
	 */
	public static boolean getConnectionStatus() {
		return connectionStatus;
	}
	
	/**
	 * Setter du statut de la connection
	 * @param connectionStatus
	 */
	public static void setConnectionStatus(Boolean connectionStatus) {
		ConnectionSQL.connectionStatus = connectionStatus;
	}


}

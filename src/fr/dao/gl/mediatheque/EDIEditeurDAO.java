package fr.dao.gl.mediatheque;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import fr.core.gl.mediatheque.Editeur;

/**
 * Class servant à la manipulation de fichier EDI
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class EDIEditeurDAO extends DAO<Editeur> {

	private Path	currentRelativePath;
	String			currentRelativePathString;
	private File	ediFile;

	public EDIEditeurDAO() {
		currentRelativePath = Paths.get("");
		currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
		ediFile = new File(currentRelativePathString + "/data/edi_editeur");
		this.read();
	}

	@Override
	public void read() {

		Scanner sc = null;
		String str;
		String nom;
		char[] editors;
		int strLength;
		Integer id = 0;

		try {
			sc = new Scanner(ediFile);
			str = sc.next();
			strLength = str.length();
			editors = str.toCharArray();
			for (int j = 0; j < strLength; j++) {
				if (editors[j] == '-') {
					editors[j] = ' ';
				}
			}
			str = String.valueOf(editors);
			for (int i = 0; i * 15 < strLength; i++) {
				nom = str.substring(i * 15, i * 15 + 14);
				id++;
				editeurHolderAll.add(new Editeur(id, nom));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		sc.close();
	}

	@Override
	public Editeur create(Editeur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Editeur update(Editeur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Editeur obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public Editeur find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}

package fr.dao.gl.mediatheque;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

import fr.core.gl.mediatheque.Usager;

/**
 * Class SQLUsager géritière de la Class DAO. Cette classe sert à la gestion des
 * Usager sur un JDBC
 * 
 * 
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 * 
 * @see DAO
 *
 */
public class SQLUsagerDAO extends DAO<Usager> {

	private static String QUERY_FIND_USAGER = "SELECT * FROM `USAGER`";

	public SQLUsagerDAO() {
		this.read();
	}

	/**
	 * Méthode réalisant un pull de la table SQL Usager. Instancie un Usager
	 * pour chaque lignes et les ajoute dans l'ArrayList contenant tout les
	 * Usagers.
	 */
	public void read() {
		try {
			statement = ConnectionSQL.getInstance().createStatement();
			ResultSet rset = statement.executeQuery(QUERY_FIND_USAGER);

			while (rset.next()) {
				Integer id = rset.getInt("ID_USAGER");
				Integer civilite = rset.getInt("CIVILITE_USAGER");
				String nom = rset.getString("NOM_USAGER");
				String prenom = rset.getString("PRENOM_USAGER");
				String adressePostale = rset.getString("ADDRPOST_USAGER");
				Date dateInscription = rset.getDate("DATEINSCRIPTION_USAGER");
				float cotisation = rset.getFloat("COTISATION_USAGER");
				Integer nombrePret = rset.getInt("NBRPRET_USAGER");
				String adresseMail = rset.getString("ADDRMAIL_USAGER");
				usagerHolderAll.add(new Usager(id, civilite, nom, prenom, adressePostale, dateInscription, cotisation,
						nombrePret, adresseMail));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Ajoute un Usager dans la base données. Une commande préparé est
	 * initialisé, ensuite l'ajout des argument est réalisé à celle-ci est
	 * éxécuté. Les différents arguments proviennent d'une instance Usager mis
	 * en paramètre de la fonction.
	 * 
	 * @param obj
	 *            Objet Usager à ajouter à la DB
	 * @return Usager
	 * @see Usager
	 * @see PreparedStatement
	 * @see Statement
	 */
	@Override
	public Usager create(Usager obj) {
		try {
			ResultSet valeursAutoGenerees = null;

			// Préparation de la commande à partir de PreparedStatement avec la
			// capacité de retrouver la clé générérer
			PreparedStatement prepare = this.connection.prepareStatement(
					"INSERT INTO `USAGER` (`CIVILITE_USAGER`, `NOM_USAGER`, `PRENOM_USAGER`, `ADDRPOST_USAGER`, "
							+ "`DATEINSCRIPTION_USAGER`, `COTISATION_USAGER`, `NBRPRET_USAGER`, `ADDRMAIL_USAGER`) "
							+ "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)");

			// Ajout des arguments
			prepare.setInt(1, obj.getCivilite());
			prepare.setString(2, obj.getNom());
			prepare.setString(3, obj.getPrenom());
			prepare.setString(4, obj.getAdressePostale());
			// TODO Fix date java / sql
			// prepare.setDate(6, obj.getDateInscription());;
			// Contournement avant le fix
			prepare.setDate(5, null);
			prepare.setFloat(6, obj.getCotisation());
			prepare.setInt(7, obj.getNombrePret());
			prepare.setString(8, obj.getAdresseEmail());

			// Execution de la commande
			prepare.executeUpdate();
			System.out.println("Usager ajouté !");

		} catch (SQLException e) {
			System.out.println("Impossible d'ajouter l'usager id : " + obj.getId() + " à la base dee données.");
			e.printStackTrace();
			return null;
		}
		return obj;
	}

	/**
	 * Suppression d'un Usager dans la table SQL USAGER à partir d'une instance
	 * Usager
	 * 
	 * @see Usager
	 */
	@Override
	public void delete(Usager obj) {
		try {
			statement.executeQuery("DELETE FROM USAGER WHERE ID_USAGER = " + obj.getId());
			// usagerHolderAll.remove(obj);
			// setUsagerHolderAll(getUsagerHolderAll().remove(obj));
			System.out.println("Utilisateur N°" + obj.getId() + " supprimé");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Recherche d'un Usager dans la table SQL à partir de son id
	 * 
	 * @see Usager
	 */
	@Override
	public Usager find(long id) {
		Usager usager = new Usager();
		try {
			ResultSet result = this.connection.createStatement()
					.executeQuery("SELECT * FROM `USAGER` WHERE ID_USAGER = " + id);
			if (result.first()) {
				// usager ;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return usager;
	}

	/**
	 * Mise à jour d'un usageer de la table SQL à partir d'une instance Usager
	 * 
	 * @see Usager
	 */
	@Override
	public Usager update(Usager obj) {
		try {
			PreparedStatement prepare = this.connection.prepareStatement(
					"UPDATE `USAGER` SET " + " CIVILITE_USAGER = ?," + " NOM_USAGER = ?" + ", PRENOM_USAGER = ?"
							+ ", ADDRPOST_USAGER = ?" + ", DATEINSCRIPTION_USAGER = ?" + ", COTISATION_USAGER = ?"
							+ ", NBRPRET_USAGER = ?" + ", ADDRMAIL_USAGER = ?" + " WHERE `ID_USAGER` = ?");

			prepare.setInt(1, obj.getCivilite());
			prepare.setString(2, obj.getNom());
			prepare.setString(3, obj.getPrenom());
			prepare.setString(4, obj.getAdressePostale());
			prepare.setDate(5, null);
			prepare.setFloat(6, obj.getCotisation());
			prepare.setInt(7, obj.getNombrePret());
			prepare.setString(8, obj.getAdresseEmail());
			prepare.setInt(9, obj.getId());
			
			// Execution de la commande
			prepare.executeUpdate();
			System.out.println("Usager " + obj.getId() + " modifier !");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

}

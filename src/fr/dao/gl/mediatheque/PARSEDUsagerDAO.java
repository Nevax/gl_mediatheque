package fr.dao.gl.mediatheque;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import fr.core.gl.mediatheque.Usager;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class PARSEDUsagerDAO extends DAO<Usager> {

	private Path	currentRelativePath;
	private String	currentRelativePathString;
	private File	parsedFile;

	public PARSEDUsagerDAO() {
		currentRelativePath = Paths.get("");
		currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
		parsedFile = new File(currentRelativePathString + "/data/parsed_usager");
		this.read();
	}

	@Override
	public void read() {
		Scanner scRecord = null;
		Integer id = 0;
		try {
			scRecord = new Scanner(parsedFile);
			while (scRecord.hasNextLine()) {
				// long aLong = scRecord.nextLong();
				String record = scRecord.nextLine();
				System.out.println(record);
				id++; // Incrémentation de l'identifiant
				// Utilisation de String.split pour charger les différent champs
				// de la ligne en utilisant comme délimiteur ";".
				// Le tout est rendu dans un tableau de String.
				String[] matches = record.split(";");
				// Instanciation d'un objet Auteur ainsi que de l'ajout dans
				// l'ArrayList
				// usagerHolderAll.add(new Usager(id, matches[0], matches[1]));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			scRecord.close();
		}
	}

	@Override
	public Usager find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usager create(Usager obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usager update(Usager obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Usager obj) {
		// TODO Auto-generated method stub

	}

}

package fr.dao.gl.mediatheque;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import fr.core.gl.mediatheque.Pret;
import fr.core.gl.mediatheque.Relance;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class SQLRelanceDAO extends DAO<Relance> {

	private static String QUERY_FIND_RELANCE = "SELECT * FROM `RELANCE`";

	public SQLRelanceDAO() {
		this.read();
	}

	@Override
	public void read() {
		try {
			statement = ConnectionSQL.getInstance().createStatement();
			ResultSet rset = statement.executeQuery(QUERY_FIND_RELANCE);

			while (rset.next()) {
				Integer id = rset.getInt("ID_RELANCE");
				Integer id_pret = rset.getInt("ID_PRET");
				// TODO Fix
				Pret pret = null;
				LocalDateTime dateRelance = LocalDateTime.now();
				// LocalDateTime dateRelance = rset.getDate("DATE_RELANCE");
				String libCourrier = rset.getString("LIB_COURRIER_RELANCE");
				Integer codeEtat = rset.getInt("CODE_ETAT_RELANCE");
				String codeMode = rset.getString("CODE_MODE_RELANCE");
				relanceHolderAll.add(new Relance(id, pret, dateRelance, libCourrier, codeEtat, codeMode));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public Relance find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Relance create(Relance obj) {
		try {
			// Préparation de la commande à partir de PreparedStatement,
			// l'identifiant est gérer par la DB
			PreparedStatement prepare = this.connection.prepareStatement(
					"INSERT INTO `RELANCE` (`ID_PRET`, `DATE_RELANCE`, `LIB_COURRIER_RELANCE`, `CODE_ETAT_RELANCE`, "
							+ "`CODE_MODE_RELANCE`) " + "VALUES ( ?, ?, ?, ?, ?)");

			// Ajout des arguments
			prepare.setInt(1, obj.getPret().getId());
			prepare.setDate(2, null);
			prepare.setString(3, obj.getLibCourrier());
			prepare.setInt(4, obj.getCodeEtat());
			prepare.setString(5, obj.getCodeMode());

			// Execution de la commande
			prepare.executeUpdate();
			System.out.println("Relance ajouté !");
		} catch (SQLException e) {
			System.out.println("Impossible d'ajouter l'usager id : " + obj.getId() + " à la base dee données.");
			e.printStackTrace();
			return null;
		}
		return obj;
	}

	@Override
	public Relance update(Relance obj) {
		try {
			PreparedStatement prepare = this.connection.prepareStatement(
					"UPDATE `RELANCE` SET " + " ID_PRET = ?," + " DATE_RELANCE = ?" + ", LIB_COURRIER_RELANCE = ?"
							+ ", CODE_ETAT_RELANCE = ?" + ", CODE_MODE_RELANCE = ?" + " WHERE `ID_RELANCE` = ?");

			prepare.setInt(1, obj.getPret().getId());
			prepare.setDate(2, null);
			prepare.setString(3, obj.getLibCourrier());
			prepare.setInt(4, obj.getCodeEtat());
			prepare.setString(5, obj.getCodeMode());
			prepare.setInt(6, obj.getId());


			// Execution de la commande
			prepare.executeUpdate();
			System.out.println("Relance " + obj.getId() + " modifier !");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

	@Override
	public void delete(Relance obj) {
		try {
			statement.executeQuery("DELETE FROM RELANCE WHERE ID_RELANCE = " + obj.getId());
			// usagerHolderAll.remove(obj);
			// setUsagerHolderAll(getUsagerHolderAll().remove(obj));
			System.out.println("Prêt N°" + obj.getId() + " supprimé");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

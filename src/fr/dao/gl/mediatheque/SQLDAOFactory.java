package fr.dao.gl.mediatheque;

import fr.core.gl.mediatheque.Auteur;
import fr.core.gl.mediatheque.Ouvrage;
import fr.core.gl.mediatheque.Editeur;
import fr.core.gl.mediatheque.Pret;
import fr.core.gl.mediatheque.Relance;
import fr.core.gl.mediatheque.Usager;

/**
 * Factory des DAO pour le SQL
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class SQLDAOFactory extends AbstractDAOFactory {

	public DAO<Usager> getUsagerDAO() {
		return new SQLUsagerDAO();
	}

	public DAO<Relance> getRelanceDAO() {
		return new SQLRelanceDAO();
	}

	public DAO<Pret> getPretDAO() {
		return new SQLPretDAO();
	}

	public DAO<Editeur> getEditeurDAO() {
		return new SQLEditeurDAO();
	}

	public DAO<Ouvrage> getOuvrageDAO() {
		return new SQLOuvrageDAO();
	}

	public DAO<Auteur> getAuteurDAO() {
		return new SQLAuteurDAO();
	}
}

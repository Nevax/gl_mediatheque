package fr.dao.gl.mediatheque;

import fr.core.gl.mediatheque.Auteur;
import fr.core.gl.mediatheque.Editeur;
import fr.core.gl.mediatheque.Ouvrage;
import fr.core.gl.mediatheque.Pret;
import fr.core.gl.mediatheque.Relance;
import fr.core.gl.mediatheque.Usager;

/**
 * Factory des DAO pour les fichiers parsé
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class PARSEDDAOFactory extends AbstractDAOFactory {

	/**
	 * Retourne une instance d'un DAO Usager
	 * @return instance d'un DAO Usager
	 */
	@Override
	public DAO<Usager> getUsagerDAO() {
		return new PARSEDUsagerDAO();
	}

	/**
	 * Retourne une instance d'un DAO Usager
	 * @return instance d'un DAO Usager
	 */
	@Override
	public DAO<Relance> getRelanceDAO() {
		return new PARSEDRelanceDAO();
	}

	/**
	 * Retourne une instance d'un DAO Usager
	 * @return instance d'un DAO Usager
	 */
	@Override
	public DAO<Pret> getPretDAO() {
		return new PARSEDPretDAO();
	}

	/**
	 * Retourne une instance d'un DAO Usager
	 * @return instance d'un DAO Usager
	 */
	@Override
	public DAO<Editeur> getEditeurDAO() {
		return new PARSEDEditeurDAO();
	}

	/**
	 * Retourne une instance d'un DAO Usager
	 * @return instance d'un DAO Usager
	 */
	@Override
	public DAO<Ouvrage> getOuvrageDAO() {
		return new PARSEDOuvrageDAO();
	}

	/**
	 * Retourne une instance d'un DAO Usager
	 * @return instance d'un DAO Usager
	 */
	@Override
	public DAO<Auteur> getAuteurDAO() {
		return new PARSEDAuteurDAO();
	}

}

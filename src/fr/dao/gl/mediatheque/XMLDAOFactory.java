package fr.dao.gl.mediatheque;

import fr.core.gl.mediatheque.Auteur;
import fr.core.gl.mediatheque.Ouvrage;
import fr.core.gl.mediatheque.Editeur;
import fr.core.gl.mediatheque.Pret;
import fr.core.gl.mediatheque.Relance;
import fr.core.gl.mediatheque.Usager;

/**
 * Factory des DAO pour les fichiers de type XML
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class XMLDAOFactory extends AbstractDAOFactory {

	public DAO<Usager> getUsagerDAO() {
		return new XMLUsagerDAO();
	}

	public DAO<Relance> getRelanceDAO() {
		return new XMLRelanceDAO();
	}

	public DAO<Pret> getPretDAO() {
		return new XMLPretDAO();
	}

	public DAO<Editeur> getEditeurDAO() {
		return new XMLEditeurDAO();
	}
	
	public DAO<Ouvrage> getOuvrageDAO() {
		return new XMLOuvrageDAO();
	}

	public DAO<Auteur> getAuteurDAO() {
		return new XMLAuteurDAO();
	}
}
package fr.dao.gl.mediatheque;

/**
 * Une énumération pouvoir controler ce que doit retourner notre factory de
 * factory
 * 
 * @see AbstractDAOFactory
 * 
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public enum FactoryType {
	SQL_DAO_FACTORY, XML_DAO_Factory, EDI_DAO_Factory, PARSED_DAO_Factory, ALL_DAO_Factory;
}

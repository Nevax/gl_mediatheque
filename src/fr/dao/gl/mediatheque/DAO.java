/**
 * 
 */
package fr.dao.gl.mediatheque;

import java.sql.Connection;
import java.sql.Statement;

import fr.core.gl.mediatheque.Mediatheque;
import fr.core.gl.mediatheque.Usager;

/**
 * Class Abstraite dont tout les DAO hériterons Hérite de la Class Mediatheque,
 * de ce fait tout les DAO hériterons de celle-ci
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 *
 */
public abstract class DAO<T> extends Mediatheque {

	/**
	 * Récupération de l'instance de la ConnectionSQl auprès du JDBC
	 */
	protected Connection	connection	= ConnectionSQL.getInstance();
	/**
	 * Création d'un Statement
	 */
	protected Statement		statement;

	/**
	 * Permet de instancier les objets à partirs de champs disponnible sur la
	 * BD.
	 */
	public abstract void read();

	/**
	 * Permet de récupérer un objet via son ID (identifiant contenue en tant que
	 * variable à l'intérieur de celui-ci.
	 * 
	 * @param id
	 *            Identifiant de l'objet
	 * @return T Une instance héritière de cette "class"
	 */
	public abstract T find(long id);

	/**
	 * Permet de créer une entrée dans la base de données par rapport à un objet
	 * 
	 * @param obj
	 *            L'objet
	 * @return Une instance héritière de cette "class"
	 */
	public abstract T create(T obj);

	/**
	 * Permet de mettre à jour les données d'une entrée dans la base
	 * 
	 * @param obj
	 *            L'objet
	 * @return T Une instance héritière de cette "class"
	 */
	public abstract T update(T obj);

	/**
	 * Permet la suppression d'une entrée de la base
	 * 
	 * @param obj
	 *            L'objet return T Une instance héritière de cette "class"
	 */
	public abstract void delete(T obj);


}

package fr.dao.gl.mediatheque;

/**
 * 
 * Super class Factory des différents ***DAOFactory
 * 
 * @see SQLDAOFactory
 * @see PARSEDDAOFactory
 * @see XMLDAOFactory
 * @see EDIDAOFactory
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 *
 */
public abstract class AbstractDAOFactory {

	public abstract DAO getUsagerDAO();

	public abstract DAO getRelanceDAO();

	public abstract DAO getPretDAO();

	public abstract DAO getEditeurDAO();

	public abstract DAO getOuvrageDAO();

	public abstract DAO getAuteurDAO();

	/**
	 * Méthode nous permettant de récupérer une factory de DAO
	 * 
	 * @param type
	 *            Type de DAO à instancier
	 * @return AbstractDAOFactory
	 */
	public static AbstractDAOFactory getFactory(FactoryType type) {

		if (type.equals(FactoryType.SQL_DAO_FACTORY))
			return new SQLDAOFactory();

		if (type.equals(FactoryType.XML_DAO_Factory))
			return new XMLDAOFactory();

		if (type.equals(FactoryType.EDI_DAO_Factory))
			return new EDIDAOFactory();

		if (type.equals(FactoryType.PARSED_DAO_Factory))
			return new PARSEDDAOFactory();

		return null;
	}
}

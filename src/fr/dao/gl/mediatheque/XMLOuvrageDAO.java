package fr.dao.gl.mediatheque;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.core.gl.mediatheque.Auteur;
import fr.core.gl.mediatheque.Editeur;
import fr.core.gl.mediatheque.Ouvrage;

/**
 * Class DAO servant à la manipulation de fichier XML pour les différents
 * Ouvrages
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class XMLOuvrageDAO extends DAO<Ouvrage> {

	/**
	 * Définie l'encodage utilisé par les fichiers XML
	 */
	static final String outputEncoding = "UTF-8";

	/**
	 * Constructeur
	 */
	public XMLOuvrageDAO() {
		this.read();
	}

	/**
	 * Lecture du fichier XML et instanciation des différents Ouvrrages
	 */
	@Override
	public void read() {

		try {

			Integer id;
			String titre;
			Boolean pretable;
			Boolean disponible;
			Integer editeurID;
			Editeur editeur;
			Integer auteurID;
			Auteur auteur;

			// Initialisation des IO
			File xmlFile = new File("data/ouvrage.xml");
			File xsdFile = new File("data/ouvrage.xsd");
			File dtdFile = new File("data/ouvrage.dtd");

			// Créé une fabrique permettan de générer les analysateurs DOM et de
			// les configurer
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			// Nous ignorons les commentaires
			dbFactory.setIgnoringComments(true);
			// Convertit les données CDATA en noeuds textuels
			dbFactory.setCoalescing(true);
			// Par défaut, pas d'espace de nommage
			dbFactory.setNamespaceAware(false);
			// Ne valide pas avec le DTD
			dbFactory.setValidating(false);

			// Utilise maintenant la fabrique pour créer un analysateur DOM,
			// c'est-à-dire une instance de DocumentBuilder
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			// Parse le contenu de xmlFile en tant que fichier XML et retourne
			// un nouveau objet Document DOM.
			Document doc = dBuilder.parse(xmlFile);
			// Normalize le document
			doc.getDocumentElement().normalize();
			// Compte le nombre d'ouvrage
			NodeList nList = doc.getElementsByTagName("ouvrage");
			// Répéter autant qu'il y a d'ouvrage
			for (int temp = 0; temp < nList.getLength(); temp++) {
				// Sélection du Node
				Node nNode = nList.item(temp);
				// Si c'est un attribut du Node, dans notre cas oui
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					// définition de l'id
					id = Integer.parseInt(eElement.getAttribute("id"));
					// Compte le nombre de titre
					NodeList ouvrageNameList = eElement.getElementsByTagName("titre");
					// Autant que le nombre d'éléments
					for (int count = 0; count < ouvrageNameList.getLength(); count++) {
						// "Curseur" sur les différents Node
						Node node1 = ouvrageNameList.item(count);
						// Si autre que le titre
						if (node1.getNodeType() == node1.ELEMENT_NODE) {
							Element elem = (Element) node1;
							// Définie le titre à partir du tag "titre"
							titre = eElement.getElementsByTagName("titre").item(0).getTextContent();

							// Définie pretable à partir du tag "pretable"
							if (eElement.getElementsByTagName("pretable").item(0).getTextContent() == "1") {
								pretable = true;
							} else {
								pretable = false;
							}

							// Définie le disponible à partir du tag
							// "disponible"
							if (eElement.getElementsByTagName("disponible").item(0).getTextContent() == "1") {
								disponible = true;
							} else {
								disponible = false;
							}
							System.out.println("disponible = " + eElement.getElementsByTagName("disponible").item(0).getTextContent());
							
							// Défnie l'éditeur à partir du tag "editeur"
							editeurID = Integer
									.parseInt(eElement.getElementsByTagName("editeur").item(0).getTextContent());
							editeur = findEditeurById(editeurID);

							// Défnie l'auteur à partir du tag "auteur"
							auteurID = Integer
									.parseInt(eElement.getElementsByTagName("auteur").item(0).getTextContent());
							auteur = findAuteurById(auteurID);

							// Instanciation de l'ouvrage et ajout dans le super
							// arraylist ouvrage
							ouvrageHolderAll.add(
									new Ouvrage(id, titre, pretable, disponible, LocalDateTime.now(), editeur, auteur));

						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Editeur findEditeurById(int id) {
		for (Editeur editeur : getEditeurHolderAll()) {
			if (editeur.getId() == id) {
				return editeur;
			}
		}
		return null;
	}

	private Auteur findAuteurById(Integer id) {
		for (Auteur auteur : getAuteurHolderAll()) {
			if (auteur.getId() == id) {
				return auteur;
			}
		}
		return null;
	}

	@Override
	public Ouvrage find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ouvrage create(Ouvrage obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ouvrage update(Ouvrage obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Ouvrage obj) {
		// TODO Auto-generated method stub

	}

}

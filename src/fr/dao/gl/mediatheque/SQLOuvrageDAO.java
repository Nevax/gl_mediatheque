package fr.dao.gl.mediatheque;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import fr.core.gl.mediatheque.Auteur;
import fr.core.gl.mediatheque.Ouvrage;
import fr.core.gl.mediatheque.Editeur;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class SQLOuvrageDAO extends DAO<Ouvrage> {

	private static String QUERY_FIND_Ouvrage = "SELECT * FROM `Ouvrage`";

	public SQLOuvrageDAO() {
		this.read();
	}

	@Override
	public void read() {
		try {
			statement = ConnectionSQL.getInstance().createStatement();
			ResultSet rset = statement.executeQuery(QUERY_FIND_Ouvrage);

			while (rset.next()) {
				Integer code = rset.getInt("CODE_Ouvrage");
				Integer id_auteur = rset.getInt("ID_AUTEUR");
				Auteur auteur = null;
				Integer id_editeur = rset.getInt("ID_EDITEUR");
				Editeur editeur = null;
				String titre = rset.getString("TITRE_Ouvrage");
				Boolean pretable = rset.getBoolean("PRETABLE_Ouvrage");
				Boolean disponnible = rset.getBoolean("DISPONNIBLE_Ouvrage");
				LocalDateTime dateAchat = LocalDateTime.now();
				// LocalDateTime dateAchat =
				// rset.getDate("DATE_ACHAT_Ouvrage");
				ouvrageHolderAll.add(new Ouvrage(code, titre, pretable, disponnible, dateAchat, editeur, auteur));
				// System.out.println(code + " " + id_auteur + " " + id_editeur
				// + " " + titre + " " + pretable + " "
				// + disponnible + " " + dateAchat);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Ouvrage find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ouvrage create(Ouvrage obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ouvrage update(Ouvrage obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Ouvrage obj) {
		// TODO Auto-generated method stub

	}

}

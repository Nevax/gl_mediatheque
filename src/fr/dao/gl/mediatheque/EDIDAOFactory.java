package fr.dao.gl.mediatheque;

import fr.core.gl.mediatheque.Auteur;
import fr.core.gl.mediatheque.Ouvrage;
import fr.core.gl.mediatheque.Editeur;
import fr.core.gl.mediatheque.Pret;
import fr.core.gl.mediatheque.Relance;
import fr.core.gl.mediatheque.Usager;

/**
 * Factory des DAO pour les fichier EDI
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class EDIDAOFactory extends AbstractDAOFactory {
	
	/**
	 * Retourne une instance d'un DAO Usager
	 * @return instance d'un DAO Usager
	 */
	public DAO<Usager> getUsagerDAO() {
		return new EDIUsagerDAO();
	}
	/**
	 * Retourne une instance d'un DAO Relance
	 * @return instance d'un DAO Relance
	 */
	public DAO<Relance> getRelanceDAO() {
		return new EDIRelanceDAO();
	}
	/**
	 * Retourne une instance d'un DAO Pret
	 * @return instance d'un DAO Pret
	 */
	public DAO<Pret> getPretDAO() {
		return new EDIPretDAO();
	}
	/**
	 * Retourne une instance d'un DAO Editeur
	 * @return instance d'un DAO Editeur
	 */
	public DAO<Editeur> getEditeurDAO() {
		return new EDIEditeurDAO();
	}
	/**
	 * Retourne une instance d'un DAO Ouvrage
	 * @return instance d'un DAO Ouvrage
	 */
	public DAO<Ouvrage> getOuvrageDAO() {
		return new EDIOuvrageDAO();
	}
	/**
	 * Retourne une instance d'un DAO Auteur
	 * @return instance d'un DAO Auteur
	 */
	public DAO<Auteur> getAuteurDAO() {
		return new EDIAuteurDAO();
	}

}

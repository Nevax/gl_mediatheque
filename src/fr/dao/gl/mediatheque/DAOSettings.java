package fr.dao.gl.mediatheque;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Class servant à la configuration des DAO. Elle s'occupe de lire le fichier de
 * configuration disponnible dans data/glmediatheque.cfg
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class DAOSettings {

	private static final String comment = "Méthode d\'accès aux données"
			+ "0 all | 1 parsed file | 2 EDI file | 3 XML | 4 SQL "
			+ "SQL Configuration"
			+ "Pour l\'utiliser avec MySQL, veuillez changer par ce driver \"org.mysql.jdbc.Driver\"";

	/**
	 * Récupère le chemin relatif de l'application
	 */
	private final static Path	currentRelativePath			= Paths.get("");

	/**
	 * Convertie le chemin en String
	 */
	private final static String	currentRelativePathString	= currentRelativePath.toAbsolutePath().toString();

	/**
	 * Création d'un Properties
	 */
	private static Properties	prop;

	/**
	 * Chemin du fichier de configuration
	 */
	private static final String	configFile					= "/data/glmediatheque.cfg";

	/**
	 * Récupère le fichier de configuration au chemin relatif data/glmediatheque.cfg
	 * 
	 * @return Properties prop Fichier de configuration
	 * @see Properties
	 * @see Path
	 */
	public static Properties getConfigFile() {

		if (prop == null) {
			prop = new Properties();
			System.out.println("Lecture du fichier de configuration");
			System.out.println("Chemin : " + currentRelativePathString);
			try {
				prop.load(new FileInputStream(currentRelativePathString + configFile));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} else {
			System.out.println("Fichier de configuration déjà chargé");
		}
		return prop;
	}

	/**
	 * Lis le fichier de configuration renvoyé par getConfigFile() et renvoi
	 * l'énumération associé à la DAO indiqué.
	 * 
	 * @return FactoryType
	 * @see FactoryType
	 */
	public static FactoryType getDAOSettings() {
		switch (getConfigFile().getProperty("mode").toString()) {
		case "0": // Default
			System.out.println("Accès DAO : Tous");
			return FactoryType.ALL_DAO_Factory;
		case "1": // Parsed file
			System.out.println("Accès DAO : PARSED");
			return FactoryType.PARSED_DAO_Factory;
		case "2": // EDI file
			System.out.println("Accès DAO : EDI");
			return FactoryType.EDI_DAO_Factory;
		case "3": // XML file
			System.out.println("Accès DAO : XML");
			return FactoryType.XML_DAO_Factory;
		case "4": // SQL DB
			System.out.println("Accès DAO : SQL");
			return FactoryType.SQL_DAO_FACTORY;
		default:
			return null;
		}
	}

	/**
	 * Configure la connextion sql à partir du fichier de configuration renvoyé par
	 * getConfigFile().
	 * 
	 * @see ConnectionSQL
	 */
	public static void readSQLSettings() {
		//getConfigFile();
		ConnectionSQL.setDb_driver(getConfigFile().getProperty("driver").toString());
		ConnectionSQL.setDb_url(getConfigFile().getProperty("host").toString());
		ConnectionSQL.setDb_login(getConfigFile().getProperty("username").toString());
		ConnectionSQL.setDb_pasword(getConfigFile().getProperty("password").toString());
		System.out.println("Password define to : "  +  getConfigFile().getProperty("password").toString());
	}

	/**
	 * 
	 * 
	 * @see ConnectionSQL
	 */
	public static void writeSQLSettings() {
		
		FileOutputStream fileOuputStream = null;
		OutputStreamWriter output = null;
		try {

			fileOuputStream = new FileOutputStream(currentRelativePathString + configFile);
			output = new OutputStreamWriter(fileOuputStream, "UTF-8");
			
			// set the properties value
			prop.setProperty("mode", "0");
			prop.setProperty("driver", ConnectionSQL.getDb_driver());
			prop.setProperty("host", ConnectionSQL.getDb_url());
			prop.setProperty("username", ConnectionSQL.getDb_login());
			System.out.println("password from prop = " + prop.getProperty("password").toString());
			System.out.println(">>>>>>>>Password from objects" + ConnectionSQL.getDb_pasword());
			prop.setProperty("password", ConnectionSQL.getDb_pasword());
			
			// save properties to project root folder
			prop.store(output, comment);
			

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public static void name(String username, String password) {
		try {
			// Properties props = new Properties();
			getConfigFile().setProperty("username", username);
			getConfigFile().setProperty("password", password);
			// getConfigFile().setProperty("ThreadCount", ""+threadCnt);
			File f = new File(currentRelativePathString + configFile);
			OutputStream out = new FileOutputStream(f);
			getConfigFile().store(out, "#This is an optional header comment string");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package fr.dao.gl.mediatheque;

import java.sql.ResultSet;
import java.sql.SQLException;

import fr.core.gl.mediatheque.Auteur;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class SQLAuteurDAO extends DAO<Auteur> {

	private static String QUERY_FIND_AUTEUR = "SELECT * FROM `AUTEUR`";

	public SQLAuteurDAO() {
		this.read();
	}

	@Override
	public void read() {
		try {
			statement = ConnectionSQL.getInstance().createStatement();
			ResultSet rset = statement.executeQuery(QUERY_FIND_AUTEUR);

			while (rset.next()) {
				Integer id = rset.getInt("ID_AUTEUR");
				String nom = rset.getString("NOM_AUTEUR");
				String prenom = rset.getString("PRENOM_AUTEUR");
				auteurHolderAll.add(new Auteur(id, nom, prenom));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public Auteur find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Auteur create(Auteur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Auteur update(Auteur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Auteur obj) {
		// TODO Auto-generated method stub

	}

}

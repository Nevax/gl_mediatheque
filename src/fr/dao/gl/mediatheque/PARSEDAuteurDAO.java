package fr.dao.gl.mediatheque;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import fr.core.gl.mediatheque.Auteur;

/**
 * Class servant à la manipulation de fichier parsé pour la class Auteur
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class PARSEDAuteurDAO extends DAO<Auteur> {

	/**
	 * Chemin relatif de l'environement d'éxécution du programme
	 */
	private Path	currentRelativePath;

	/**
	 * Contenant le chemin relatif sous forme de String
	 */
	String			currentRelativePathString;

	/**
	 * Fichier parsé utilisé par ce DAO
	 */
	private File	parsedFile;

	/**
	 * Constructeur de la Class PARSEDAuteurDAO
	 */
	public PARSEDAuteurDAO() {
		// récupère le chemin relatif
		currentRelativePath = Paths.get("");
		// Le convertie en String
		currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
		// Définie le chemin du fichier parsé utilisé par ce DAO
		parsedFile = new File(currentRelativePathString + "/data/parsed_auteur");
		this.read();
	}

	/**
	 * Récupération des données aupès du fichier parsé puis appel le
	 * constructeur Auteur avec les données récupéré
	 */
	@Override
	public void read() {
		Scanner scRecord = null;
		try {
			scRecord = new Scanner(parsedFile);
			while (scRecord.hasNextLine()) {
				String record = scRecord.nextLine();
				// Utilisation de String.split pour charger les différent champs
				// de la ligne en utilisant comme délimiteur ";".
				// Le tout est rendu dans un tableau de String.
				String[] matches = record.split(";");
				// Instanciation d'un objet Auteur ainsi que de l'ajout dans
				// l'ArrayList
				auteurHolderAll.add(new Auteur(Integer.parseInt(matches[0]), matches[1], matches[2]));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			scRecord.close();
		}
	}

	@Override
	public Auteur find(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Auteur create(Auteur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Auteur update(Auteur obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Auteur obj) {
		// TODO Auto-generated method stub

	}

}

package fr.views.gl.mediatheque;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.sun.corba.se.impl.ior.GenericTaggedComponent;

import fr.core.gl.mediatheque.Mediatheque;
import fr.dao.gl.mediatheque.AbstractDAOFactory;
import fr.dao.gl.mediatheque.ConnectionSQL;
import fr.dao.gl.mediatheque.DAOSettings;
import fr.dao.gl.mediatheque.FactoryType;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JProgressBar;

public class MainFrame extends Mediatheque {

	private JFrame				mFrame;
	private JTable				mTable;
	private JPanel				mPanel;
	private JScrollPane			mScrollPane;
	private DefaultTableModel	mModel;
	private JLabel				lbl_bigLogo;
	private JButton				btn_website;

	private static int			X_FRAME_POSITION	= 100;
	private static int			Y_FRAME_POSITION	= 100;
	private static int			WIDTH_FRAME			= 1000;
	private static int			HEIGHT_FRAME		= 600;
	private static String		NAME_FRAME			= "Mediathèque Grand Lyon Métropole";
	private JProgressBar		progressBarEnvoi;
	private JButton				btnValiderEnvoi;
	private JButton				btnAnnulerEnvoi;
	private JLabel				lblInfoProgressBar;
	private JButton btnConnection ;
	private JLabel lblCurrentStatusConnection;

	/**
	 * Lance l'application
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.mFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Créé l'application
	 */
	public MainFrame() {
		initialize();
		// sync();
		// sync();
		// sync();
		// sync();
	}

	private void sync() {
		
		if (DAOSettings.getDAOSettings() == FactoryType.ALL_DAO_Factory) {
			usagerdao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getUsagerDAO();
			ouvragedao = AbstractDAOFactory.getFactory(FactoryType.XML_DAO_Factory).getOuvrageDAO();
			auteurdao = AbstractDAOFactory.getFactory(FactoryType.PARSED_DAO_Factory).getAuteurDAO();
			pretdao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getPretDAO();
			relancedao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getRelanceDAO();
			editeurdao = AbstractDAOFactory.getFactory(FactoryType.EDI_DAO_Factory).getEditeurDAO();
		} else {
			usagerdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getUsagerDAO();
			ouvragedao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getOuvrageDAO();
			auteurdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getAuteurDAO();
			pretdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getPretDAO();
			relancedao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getRelanceDAO();
			editeurdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getEditeurDAO();
		}
		btnConnection.setVisible(false);
		lblCurrentStatusConnection.setIcon(new ImageIcon(MainFrame.class.getResource("/fr/ressources/gl/mediatheque/connected.png")));
		ConnectionSQL.setConnectionStatus(true);
	}

	/**
	 * Initialise le contenue de la frame
	 */

	private void initialize() {
		mFrame = new JFrame();
		mFrame.setTitle(NAME_FRAME);
		mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mFrame.setBounds(X_FRAME_POSITION, Y_FRAME_POSITION, WIDTH_FRAME, HEIGHT_FRAME);
		mFrame.getContentPane().setLayout(new GridLayout(5, 5));
		mFrame.setMinimumSize(new Dimension(500, 400));

		mPanel = new JPanel();

		mPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		mFrame.setContentPane(mPanel);

		JMenuBar menuBar = new JMenuBar();

		JMenu mnSettings = new JMenu("Paramètres");
		JMenuItem mntmSettingsLogin = new JMenuItem("Identifiant");
		JMenuItem mntmSync = new JMenuItem("Synchroniser");
		JMenuItem mntmQuitter = new JMenuItem("Quitter");

		mFrame.setJMenuBar(menuBar);
		JMenu mnShow = new JMenu("Voir");
		JMenuItem mntmDocuments = new JMenuItem("Documents");
		JMenuItem mntmUsagers = new JMenuItem("Usagers");
		JMenuItem mntmSorties = new JMenuItem("Sorties");
		menuBar.add(mnShow);
		mnShow.add(mntmDocuments);
		mnShow.add(mntmUsagers);
		mnShow.add(mntmSorties);

		mntmDocuments.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showScrollTable();
				mTable.setModel(new DefaultTableModel(new Object[][] {},
						new String[] { "Code", "Titre", "Auteur", "Editeur", "Pretable", "Disponible", "Date achat" }));
				mModel = (DefaultTableModel) mTable.getModel();
				for (int i = 0; i < getOuvrageHolderAll().size(); i++) {
					if (getOuvrageHolderAll().get(i).getEditeur() != null
							|| getOuvrageHolderAll().get(i).getAuteur() != null) {
						mModel.addRow(new Object[] { getOuvrageHolderAll().get(i).getCode(),
								getOuvrageHolderAll().get(i).getTitre(),
								getOuvrageHolderAll().get(i).getAuteur().getNom() + " "
										+ getOuvrageHolderAll().get(i).getAuteur().getPrenom(),
								getOuvrageHolderAll().get(i).getEditeur().getNom(),
								getOuvrageHolderAll().get(i).getPretableToString(),
								getOuvrageHolderAll().get(i).getDisponibleToString(),
								getOuvrageHolderAll().get(i).getDateAchat() });
					}
				}
				mPanel.revalidate();
				mPanel.repaint();
			}
		});

		mntmUsagers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showScrollTable();
				mTable.setModel(new DefaultTableModel(new Object[][] {},
						new String[] { "Identifiant", "Civilité", "Nom", "Prénom", "Adresse" }));
				mModel = (DefaultTableModel) mTable.getModel();
				for (int i = 0; i < getUsagerHolderAll().size(); i++) {
					mModel.addRow(new Object[] { getUsagerHolderAll().get(i).getId(),
							getUsagerHolderAll().get(i).getCiviliteToString(), getUsagerHolderAll().get(i).getNom(),
							getUsagerHolderAll().get(i).getPrenom(), getUsagerHolderAll().get(i).getAdressePostale() });
				}
				mPanel.revalidate();
				mPanel.repaint();
			}
		});

		mntmSorties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showScrollTable();
				mTable.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Identifiant", "Usagé",
						"Code Document", "Titre", "Date sortie prêt", "Date retour prêt", "Prêt retourné" }));
				mModel = (DefaultTableModel) mTable.getModel();
				for (int i = 0; i < getPretHolderAll().size(); i++) {
					if (getPretHolderAll().get(i).getUsager() != null
							|| getPretHolderAll().get(i).getOuvrage() != null) {
						mModel.addRow(new Object[] { getPretHolderAll().get(i).getId(),
								getPretHolderAll().get(i).getUsager().getNom() + " "
										+ getPretHolderAll().get(i).getUsager().getPrenom(),
								getPretHolderAll().get(i).getOuvrage().getCode(),
								getPretHolderAll().get(i).getOuvrage().getTitre(),
								getPretHolderAll().get(i).getDateSortie(), getPretHolderAll().get(i).getDateRetour(),
								getPretHolderAll().get(i).getDispo() });
					}
				}
				mPanel.revalidate();
				mPanel.repaint();
			}
		});

		menuBar.add(mnSettings);
		mnSettings.add(mntmSettingsLogin);
		mnSettings.add(mntmSync);
		mnSettings.add(mntmQuitter);

		btn_website = new JButton("");
		btn_website.setHorizontalAlignment(SwingConstants.CENTER);
		btn_website.setHorizontalTextPosition(SwingConstants.CENTER);
		btn_website.setBorderPainted(false);
		btn_website.setContentAreaFilled(false);
		btn_website.setFocusPainted(false);
		btn_website.setOpaque(false);
		btn_website.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_website.setIcon(new ImageIcon(MainFrame.class.getResource("/fr/ressources/gl/mediatheque/mini_logo.png")));

		lbl_bigLogo = new JLabel("");
		lbl_bigLogo.setHorizontalTextPosition(SwingConstants.CENTER);
		lbl_bigLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_bigLogo.setIcon(new ImageIcon(MainFrame.class.getResource("/fr/ressources/gl/mediatheque/logo.png")));

		mntmSettingsLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LoginFrameSQL();
			}
		});

		mntmQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mFrame.dispose();
			}
		});

		mntmSync.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sync();
			}
		});

		btn_website.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URL("http://www.grandlyon.com").toURI());
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		btnValiderEnvoi = new JButton("Valider");
		btnAnnulerEnvoi = new JButton("Annuler");
		progressBarEnvoi = new JProgressBar();
		lblInfoProgressBar = new JLabel("A valider pour envoi");

		btnValiderEnvoi.setVisible(false);
		btnAnnulerEnvoi.setVisible(false);
		progressBarEnvoi.setVisible(false);
		lblInfoProgressBar.setVisible(false);

		btnConnection = new JButton("Connection");
		btnConnection.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				sync();
				sync();
			}
		});
		
		lblCurrentStatusConnection = new JLabel("");
		lblCurrentStatusConnection.setIcon(new ImageIcon(MainFrame.class.getResource("/fr/ressources/gl/mediatheque/disconnected.png")));

		GroupLayout gl_contentPane = new GroupLayout(mPanel);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(34)
					.addComponent(lbl_bigLogo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(56))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblCurrentStatusConnection)
					.addGap(30)
					.addComponent(progressBarEnvoi, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblInfoProgressBar)
					.addGap(272)
					.addComponent(btnAnnulerEnvoi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnValiderEnvoi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btn_website, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(401)
					.addComponent(btnConnection)
					.addContainerGap(475, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(99)
					.addComponent(lbl_bigLogo, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btn_website)
						.addComponent(lblCurrentStatusConnection)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnConnection)
									.addPreferredGap(ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
										.addComponent(btnValiderEnvoi)
										.addComponent(btnAnnulerEnvoi)))
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(lblInfoProgressBar)
									.addComponent(progressBarEnvoi, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addContainerGap())))
		);
		mPanel.setLayout(gl_contentPane);

		// //////////////////////
		// showScrollTable();
		// mTable.setModel(
		// new DefaultTableModel(new Object[][] {}, new String[] { "Identifiant",
		// "Usagé",
		// "Code Document", "Titre", "Date sortie prêt", "Date retour prêt", "Prêt
		// retourné" }));
		// mModel = (DefaultTableModel) mTable.getModel();
		// for (int i = 0; i < getPretHolderAll().size(); i++) {
		// if (getPretHolderAll().get(i).getUsager() != null
		// || getPretHolderAll().get(i).getOuvrage() != null) {
		// mModel.addRow(new Object[] { getPretHolderAll().get(i).getId(),
		// getPretHolderAll().get(i).getUsager().getNom() + " " +
		// getPretHolderAll().get(i).getUsager().getPrenom(),
		// getPretHolderAll().get(i).getOuvrage().getCode(),
		// getPretHolderAll().get(i).getOuvrage().getTitre(),
		// getPretHolderAll().get(i).getDateSortie(),
		// getPretHolderAll().get(i).getDateRetour(),
		// getPretHolderAll().get(i).getDispo() });
		// }
		// }
		// mPanel.revalidate();
		// mPanel.repaint();
		// /////////////////////

	}

	public void showScrollTable() {

		if (mTable != null) {
			mTable = null;
		}
		if (mScrollPane != null) {
			mPanel.remove(mScrollPane);
			mScrollPane = null;
		}
		mPanel.revalidate();
		mPanel.repaint();
		mTable = new JTable();
		mFrame.getContentPane().add(mTable, BorderLayout.CENTER);

		mTable.setAutoCreateRowSorter(true);
		mScrollPane = new JScrollPane(mTable);
		Dimension sizeOfWindow = mFrame.getBounds().getSize();
		sizeOfWindow.setSize(sizeOfWindow.width, sizeOfWindow.height - 100);
		mScrollPane.setSize(sizeOfWindow);

		mPanel.add(mScrollPane);
		mPanel.remove(lbl_bigLogo);

		btnValiderEnvoi.setVisible(true);
		btnAnnulerEnvoi.setVisible(true);
		progressBarEnvoi.setVisible(true);
		lblInfoProgressBar.setVisible(true);

		mTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = mTable.rowAtPoint(e.getPoint());
				int col = mTable.columnAtPoint(e.getPoint());

				if (row >= 0 && col >= 0) {
					System.out.println("row = " + row + " ; col = " + col);

				}

				if (col == 0) {
					String id = mTable.getModel().getValueAt(row, col).toString();
					new UsagerDetails(id);
				}
			}
		});
	}
}

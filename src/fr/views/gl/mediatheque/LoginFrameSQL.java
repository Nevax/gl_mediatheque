package fr.views.gl.mediatheque;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import fr.dao.gl.mediatheque.ConnectionSQL;
import fr.dao.gl.mediatheque.DAOSettings;

import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginFrameSQL extends MainFrame {

	private Boolean			connected	= false;

	private JFrame			loginFrame;
	private JTextField		txbLogin;
	private JPasswordField	pwdMDP;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrameSQL window = new LoginFrameSQL();
					window.loginFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginFrameSQL() {
		DAOSettings.readSQLSettings();
		initialize();
		loginFrame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		loginFrame = new JFrame();
		loginFrame.setTitle("Identification");
		loginFrame.setBounds(100, 100, 350, 140);
		loginFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		txbLogin = new JTextField();
		txbLogin.setText(ConnectionSQL.getDb_login());
		System.out.println(ConnectionSQL.getDb_login());
		txbLogin.setColumns(10);

		pwdMDP = new JPasswordField(ConnectionSQL.getDb_pasword());
		pwdMDP.setToolTipText("");
		pwdMDP.setColumns(10);

		JLabel lblLogin = new JLabel("Login :");
		JLabel lblPasswrd = new JLabel("Mot de passe :");
		JButton btnAnnuler = new JButton("Annuler");
		JButton btnValider = new JButton("Valider");

		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loginFrame.dispose();
			}
		});

		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.out.println("'''''''''''''''''''''''''''''''''''''passwird = " + pwdMDP.getPassword().toString());
				DAOSettings.readSQLSettings();
//				ConnectionSQL.setDb_login(txbLogin.getSelectedText());
//				ConnectionSQL.setDb_pasword(pwdMDP.getSelectedText());
				ConnectionSQL.setDb_login(""+txbLogin.getText());
				ConnectionSQL.setDb_pasword(""+pwdMDP.getPassword().toString());
				DAOSettings.writeSQLSettings();
				if (ConnectionSQL.getInstance() != null) {
					if (!ConnectionSQL.getConnectionStatus()) {
						JOptionPane.showMessageDialog(null, "Connexion réussi.", "Connexion réussi",
								JOptionPane.PLAIN_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Connexion échoué.", "Connexion échoué",
								JOptionPane.ERROR_MESSAGE);
					}
					connected = true;
				} else {
					JOptionPane.showMessageDialog(null, "La connexion a échoué.", "Erreur connexion",
							JOptionPane.ERROR_MESSAGE);
				}
				loginFrame.dispose();
			}
		});
		GroupLayout groupLayout = new GroupLayout(loginFrame.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(groupLayout
				.createSequentialGroup().addGap(18)
				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup().addComponent(btnAnnuler)
								.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnValider))
						.addGroup(Alignment.LEADING,
								groupLayout.createSequentialGroup()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblPasswrd).addComponent(lblLogin))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(txbLogin, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
												.addComponent(pwdMDP, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE))))
				.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addGap(16)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblLogin).addComponent(
						txbLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(pwdMDP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPasswrd))
				.addGap(8).addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(btnValider)
						.addComponent(btnAnnuler))
				.addGap(27)));
		loginFrame.getContentPane().setLayout(groupLayout);
		// Placer au millieu dee l'écran si null
		loginFrame.setLocationRelativeTo(null);
	}

	public Boolean getResultLogin() {
		return connected;
	}
}

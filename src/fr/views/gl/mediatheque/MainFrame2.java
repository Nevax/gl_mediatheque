package fr.views.gl.mediatheque;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import fr.core.gl.mediatheque.Mediatheque;
import fr.dao.gl.mediatheque.AbstractDAOFactory;
import fr.dao.gl.mediatheque.DAOSettings;
import fr.dao.gl.mediatheque.FactoryType;

public class MainFrame2 extends Mediatheque {

	private JFrame				mFrame;
	private JTable				mTable;
	private JPanel				mPanel;
	private JScrollPane			mScrollPane;
	private DefaultTableModel	mModel;
	private JButton				btn_website;

	private static int			X_FRAME_POSITION	= 100;
	private static int			Y_FRAME_POSITION	= 100;
	private static int			WIDTH_FRAME			= 1000;
	private static int			HEIGHT_FRAME		= 600;
	private static String		NAME_FRAME			= "Mediathèque Grand Lyon Métropole";

	/**
	 * Lance l'application
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame2 window = new MainFrame2();
					window.mFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Créé l'application
	 */
	public MainFrame2() {
		initialize();
		sync();
		sync();
		sync();
		sync();
	}

	private void sync() {
		if (DAOSettings.getDAOSettings() == FactoryType.ALL_DAO_Factory) {
			usagerdao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getUsagerDAO();
			ouvragedao = AbstractDAOFactory.getFactory(FactoryType.XML_DAO_Factory).getOuvrageDAO();
			auteurdao = AbstractDAOFactory.getFactory(FactoryType.PARSED_DAO_Factory).getAuteurDAO();
			pretdao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getPretDAO();
			relancedao = AbstractDAOFactory.getFactory(FactoryType.SQL_DAO_FACTORY).getRelanceDAO();
			editeurdao = AbstractDAOFactory.getFactory(FactoryType.EDI_DAO_Factory).getEditeurDAO();
		} else {
			usagerdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getUsagerDAO();
			ouvragedao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getOuvrageDAO();
			auteurdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getAuteurDAO();
			pretdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getPretDAO();
			relancedao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getRelanceDAO();
			editeurdao = AbstractDAOFactory.getFactory(DAOSettings.getDAOSettings()).getEditeurDAO();
		}
	}

	/**
	 * Initialise le contenue de la frame
	 */

	private void initialize() {
		mFrame = new JFrame();
		mFrame.setTitle(NAME_FRAME);
		mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mFrame.setBounds(X_FRAME_POSITION, Y_FRAME_POSITION, WIDTH_FRAME, HEIGHT_FRAME);
		mFrame.getContentPane().setLayout(new GridLayout(5,5));

		mPanel = new JPanel();

		mFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
			}
		});

		mPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		mFrame.setContentPane(mPanel);

		JMenuBar menuBar = new JMenuBar();

		JMenu mnShow = new JMenu("Voir");
		JMenuItem mntmDocuments = new JMenuItem("Documents");
		JMenuItem mntmUsagers = new JMenuItem("Usagers");
		JMenuItem mntmSorties = new JMenuItem("Sorties");

		JMenu mnSettings = new JMenu("Paramètres");
		JMenuItem mntmSettingsDatabase = new JMenuItem("Base de données");
		JMenuItem mntmSync = new JMenuItem("Sync");
		JMenuItem mntmQuitter = new JMenuItem("Quitter");

		mFrame.setJMenuBar(menuBar);

		menuBar.add(mnShow);
		mnShow.add(mntmDocuments);
		mnShow.add(mntmUsagers);
		mnShow.add(mntmSorties);

		menuBar.add(mnSettings);
		mnSettings.add(mntmSettingsDatabase);
		mnSettings.add(mntmSync);
		mnSettings.add(mntmQuitter);

		btn_website = new JButton("");
		btn_website.setHorizontalAlignment(SwingConstants.CENTER);
		btn_website.setHorizontalTextPosition(SwingConstants.CENTER);
		btn_website.setBorderPainted(false);
		btn_website.setContentAreaFilled(false);
		btn_website.setFocusPainted(false);
		btn_website.setOpaque(false);
		btn_website.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_website.setIcon(new ImageIcon(MainFrame2.class.getResource("/fr/ressources/gl/mediatheque/mini_logo.png")));

		mntmSettingsDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LoginFrameSQL();
			}
		});

		mntmQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mFrame.dispose();
			}
		});

		mntmSync.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sync();
			}
		});

		btn_website.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URL("http://www.grandlyon.com").toURI());
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		mntmDocuments.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showScrollTable();
				mTable.setModel(new DefaultTableModel(new Object[][] {},
						new String[] { "Code", "Titre", "Auteur", "Editeur", "Pretable", "Disponible", "Date achat" }));
				mModel = (DefaultTableModel) mTable.getModel();
				for (int i = 0; i < getOuvrageHolderAll().size(); i++) {
					if (getOuvrageHolderAll().get(i).getEditeur() != null
							|| getOuvrageHolderAll().get(i).getAuteur() != null) {
						mModel.addRow(new Object[] { getOuvrageHolderAll().get(i).getCode(),
								getOuvrageHolderAll().get(i).getTitre(),
								getOuvrageHolderAll().get(i).getAuteur().getNom() + " "
										+ getOuvrageHolderAll().get(i).getAuteur().getPrenom(),
								getOuvrageHolderAll().get(i).getEditeur().getNom(),
								getOuvrageHolderAll().get(i).getPretableToString(),
								getOuvrageHolderAll().get(i).getDisponibleToString(),
								getOuvrageHolderAll().get(i).getDateAchat() });
					}
				}
				mPanel.revalidate();
				mPanel.repaint();
			}
		});

		mntmUsagers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showScrollTable();
				mTable.setModel(new DefaultTableModel(new Object[][] {},
						new String[] { "Identifiant", "Civilité", "Nom", "Prénom", "Adresse" }));
				mModel = (DefaultTableModel) mTable.getModel();
				for (int i = 0; i < getUsagerHolderAll().size(); i++) {
					mModel.addRow(new Object[] { getUsagerHolderAll().get(i).getId(),
							getUsagerHolderAll().get(i).getCiviliteToString(), getUsagerHolderAll().get(i).getNom(),
							getUsagerHolderAll().get(i).getPrenom(), getUsagerHolderAll().get(i).getAdressePostale() });
				}
				mPanel.revalidate();
				mPanel.repaint();
			}
		});

		mntmSorties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showScrollTable();
				mTable.setModel(
						new DefaultTableModel(new Object[][] {}, new String[] { "Identifiant", "Identifiant Usagé",
								"Code Document", "Date sortie prêt", "Date retour prêt", "Prêt retourné" }));
				mModel = (DefaultTableModel) mTable.getModel();
				for (int i = 0; i < getPretHolderAll().size(); i++) {
					if (getPretHolderAll().get(i).getUsager() != null
							|| getPretHolderAll().get(i).getOuvrage() != null) {
						mModel.addRow(new Object[] { getPretHolderAll().get(i).getId(),
								getPretHolderAll().get(i).getUsager().getId(),
								getPretHolderAll().get(i).getOuvrage().getCode(),
								getPretHolderAll().get(i).getDateSortie(), getPretHolderAll().get(i).getDateRetour(),
								getPretHolderAll().get(i).getDispo() });
					}
				}
				mPanel.revalidate();
				mPanel.repaint();
			}
		});

		GroupLayout gl_contentPane = new GroupLayout(mPanel);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(841, Short.MAX_VALUE)
					.addComponent(btn_website, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(494, Short.MAX_VALUE)
					.addComponent(btn_website)
					.addContainerGap())
		);
		mPanel.setLayout(gl_contentPane);
		
		
		
		

		////////////////////////////////
		showScrollTable();
		mTable.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Identifiant", "Civilité", "Nom", "Prénom", "Adresse" }));
		mModel = (DefaultTableModel) mTable.getModel();
		for (int i = 0; i < getUsagerHolderAll().size(); i++) {
			mModel.addRow(new Object[] { getUsagerHolderAll().get(i).getId(),
					getUsagerHolderAll().get(i).getCiviliteToString(), getUsagerHolderAll().get(i).getNom(),
					getUsagerHolderAll().get(i).getPrenom(), getUsagerHolderAll().get(i).getAdressePostale() });
		}
		mPanel.revalidate();
		mPanel.repaint();
		/////////////////////////////////
	}
	
	public void showScrollTable() {
		
		
		
		if (mTable != null) {
			mTable = null;
		}
		if (mScrollPane != null) {
			mPanel.remove(mScrollPane);
			mScrollPane = null;
		}
		mPanel.revalidate();
		mPanel.repaint();
		mTable = new JTable();
		mTable.setEnabled(false);
		mFrame.getContentPane().add(mTable, BorderLayout.CENTER);
		mFrame.setMinimumSize(new Dimension(200, 200));
		mTable.setAutoCreateRowSorter(true);
		mScrollPane = new JScrollPane(mTable);
		Dimension sizeOfWindow = mFrame.getBounds().getSize();
		sizeOfWindow.setSize(sizeOfWindow.width-200, sizeOfWindow.height);
		mScrollPane.setSize(sizeOfWindow);

		mPanel.add(mScrollPane);
		
		

		
	}
}

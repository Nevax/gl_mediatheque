package fr.io.gl.mediatheque;

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class XmlValidator {
	public static void main(String args[]) {
		XSDValidator xsd = new XSDValidator();
		System.out.println(xsd.validateXMLSchema("data/Ouvrage.xml", "data/Ouvrage.xsl"));
	}
}

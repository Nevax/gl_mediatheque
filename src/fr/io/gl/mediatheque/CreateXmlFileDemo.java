package fr.io.gl.mediatheque;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

//https://www.tutorialspoint.com/java_xml/java_dom_query_Ouvrage.htm

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class CreateXmlFileDemo {

	public static void main(String argv[]) {

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			// root element
			Element rootElement = doc.createElement("root");
			doc.appendChild(rootElement);

			// Ouvrage element
			Element Ouvrage = doc.createElement("Ouvrage");
			rootElement.appendChild(Ouvrage);

			// setting attribute to element
			Attr attr = doc.createAttribute("id");
			attr.setValue("001");
			Ouvrage.setAttributeNode(attr);

			// carname element
			Element title = doc.createElement("titre");
			// Attr attrType = doc.createAttribute("type");
			// attrType.setValue("formula one");
			// carname.setAttributeNode(attrType);
			title.appendChild(doc.createTextNode("Vingt Mille Lieues sous les mers"));
			Ouvrage.appendChild(title);

			Element title1 = doc.createElement("pretable");
			// Attr attrType1 = doc.createAttribute("type");
			// attrType1.setValue("sports");
			// carname1.setAttributeNode(attrType1);
			title1.appendChild(doc.createTextNode("true"));
			Ouvrage.appendChild(title1);

			Element title2 = doc.createElement("disponible");
			// Attr attrType1 = doc.createAttribute("type");
			// attrType1.setValue("sports");
			// carname1.setAttributeNode(attrType1);
			title2.appendChild(doc.createTextNode("true"));
			Ouvrage.appendChild(title2);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("data/Ouvrage.xml"));
			transformer.transform(source, result);

			// Output to console for testing
			// StreamResult consoleResult = new StreamResult(System.out);
			// transformer.transform(source, consoleResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

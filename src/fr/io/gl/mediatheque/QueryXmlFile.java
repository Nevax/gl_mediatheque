package fr.io.gl.mediatheque;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//https://docs.oracle.com/javase/tutorial/jaxp/dom/readingXML.html

/**
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class QueryXmlFile {

	static final String			outputEncoding	= "UTF-8";
	private ArrayList<String>	returnQuery		= new ArrayList<String>();

	public ArrayList<String> queryOuvrageXml() {

		try {
			// init IO
			File xmlFile = new File("data/Ouvrage.xml");
			File xsdFile = new File("data/Ouvrage.xsd");
			// File dtdFile = new File("data/Ouvrage.dtd");

			// Créé une fabrique permettan de générer les analysateurs DOM et de
			// les configurer
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setIgnoringComments(true); // Nous ignorons les
													// commentaires
			dbFactory.setCoalescing(true); // Convertit les données CDATA en
											// noeuds textuels
			dbFactory.setNamespaceAware(false); // Par défaut, pas d'espace de
												// nommage
			dbFactory.setValidating(true); // Valide avec le DTD

			// Utilise maintenant la fabrique pour créer un analysateur dOM,
			// c'est-à-dire une instance de OuvrageBuilder
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			// optional, but recommended
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			System.out.println("Root element : " + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("Ouvrage");
			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				System.out.print("\nCurrent Element : ");
				System.out.println(nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.print("id : ");
					System.out.println(eElement.getAttribute("id"));

					NodeList OuvrageNameList = eElement.getElementsByTagName("titre");

					for (int count = 0; count < OuvrageNameList.getLength(); count++) {
						Node node1 = OuvrageNameList.item(count);
						if (node1.getNodeType() == node1.ELEMENT_NODE) {
							Element car = (Element) node1;

							System.out.println(
									"Tritre : " + eElement.getElementsByTagName("titre").item(0).getTextContent());
							System.out.println(
									"pretable : " + eElement.getElementsByTagName("pretable").item(0).getTextContent());
							System.out.println("disponible : "
									+ eElement.getElementsByTagName("disponible").item(0).getTextContent());

						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnQuery;

	}

	public static void main(String argv[]) {

		QueryXmlFile q = new QueryXmlFile();
		q.queryOuvrageXml();
	}
}

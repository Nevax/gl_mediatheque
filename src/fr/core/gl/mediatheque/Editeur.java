package fr.core.gl.mediatheque;

/**
 * Class Editeur pour les différents Editeurs des Ouvrages
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class Editeur {

	private Integer	id;
	/** Nom de l'editeur */
	private String	nom;

	/**
	 * Constructeur de la Classe Editeur
	 * 
	 * @param id
	 *            identifiant de l'Editeur
	 * @param nom
	 *            Nom de l'Editeur
	 */
	public Editeur(Integer id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	
	public Editeur() {
		this.id = 0;
		this.nom = "unknown";
	}

	/**
	 * Getter du nom de l'Editeur
	 * 
	 * @return id Nom de l'Editeur
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Setter du nom de l'Editeur
	 * 
	 * @param id
	 *            Nom de l'Editeur
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Getter du nom de l'Editeur
	 * 
	 * @return nom Nom de l'Editeur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter du nom de l'Editeur
	 * 
	 * @param nom
	 *            Nom de l'editeur
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

}

package fr.core.gl.mediatheque;

import java.util.ArrayList;

/**
 * Class implémente un auteur ayant écrit au moins un ouvrage présent dans la
 * mediatheque
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class Auteur {
	/** Identifiant de l'auteur */
	private Integer				id;
	/** Nom de l'auteur */
	private String				nom;
	/** Prenom de l'auteur */
	private String				prenom;
	/**
	 * ArrayList contenant tous les Ouvrage relié à cette auteur.
	 * 
	 * @see ArrayList
	 * @see Ouvrage
	 */
	private ArrayList<Ouvrage>	OuvrageHolder;

	/**
	 * Constructeur de la class Auteur, ayant pour paramètre l'identifiant de
	 * l'auteur, son nom, son prénom. Une ArrayList OuvrageHolder est instancié
	 * contenant tout les Ouvrages de l'auteur.
	 * 
	 * @param id
	 *            Identifiant de l'auteur
	 * @param nom
	 *            Nom de l'auteur
	 * @param prenom
	 *            Prénom de l'auteur
	 * @see Auteur
	 */
	public Auteur(Integer id, String nom, String prenom) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		OuvrageHolder = new ArrayList<Ouvrage>();
	}
	
	public Auteur() {
		this.id = 0;
		this.nom = "unknown";
		this.prenom = "unknown";
		OuvrageHolder = new ArrayList<Ouvrage>();
	}

	/**
	 * (Getter) Fonction retournant la liste des Ouvrages relié à cet auteur.
	 * 
	 * @return OuvrageHolder Retourne une ArrayList composé de Ouvrage.
	 */
	public ArrayList<Ouvrage> getOuvrageHolder() {
		return OuvrageHolder;
	}

	/**
	 * (Getter) Méthode définissant l'ArrayList OuvrageHolder composé de
	 * Ouvrage.
	 * 
	 * @param OuvrageHolder
	 *            ArrayList composé de Ouvrage
	 */
	public void setOuvrageHolder(ArrayList<Ouvrage> OuvrageHolder) {
		this.OuvrageHolder = OuvrageHolder;
	}

	/**
	 * Getter du nom de l'Auteur
	 * 
	 * @return nom Nom de l'auteur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter du nom de l'Auteur
	 * 
	 * @param nom
	 *            Nom de l'auteur
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Getter du prénom de l'Auteur
	 * 
	 * @return prenom Prénom de l'Auteur
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Setter du prénom de l'Auteur
	 * 
	 * @param prenom
	 *            Prénom de l'auteur
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Getter de l'identifiant de l'Auteur
	 * 
	 * @return id Identifiant de l'auteur
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter de l'identifiant de l'Auteur
	 * 
	 * @param id
	 *            Identifiant de l'Auteur
	 */
	public void setId(int id) {
		this.id = id;
	}

}

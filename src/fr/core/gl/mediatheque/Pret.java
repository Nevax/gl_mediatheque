package fr.core.gl.mediatheque;

import static java.lang.Math.toIntExact;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 * Class Pret servant à stocker tout les prets en cours.
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class Pret {

	/** identifiant du Pret */
	private int					id;
	/** Date de sortie du pret */
	private LocalDateTime		dateSortie;
	/** Date de retour du pret */
	private LocalDateTime		dateRetour;
	/** Si le Pret a été retourné */
	private boolean				retour;
	/** Contient tous les relances de ce prêt */
	public ArrayList<Relance>	relanceHolder;
	/** L'uager relié à ce pret */
	private Usager				usager;
	/** Contient le Ouvrage relié à ce pret */
	private Ouvrage			Ouvrage;

	/**
	 * Constructeur de Pret
	 * 
	 * @param id
	 *            Identifiant du Pret
	 * @param usager
	 *            Usager ayant réalisé ce Pret
	 * @param Ouvrage
	 *            Ouvrage associé à ce Pret
	 * @param dateSortie
	 *            Date de sortie de ce Pret
	 * @param dateRetour
	 *            Date de retour de ce Pret
	 * @param retour
	 *            Si le près à était retourné ou non
	 * @see Usager
	 * @see Ouvrage
	 */
	public Pret(Integer id, Usager usager, Ouvrage Ouvrage, LocalDateTime dateSortie, LocalDateTime dateRetour,
			Boolean retour) {
		this.id = id;
		this.usager = usager;
		this.Ouvrage = Ouvrage;
		this.dateSortie = dateSortie;
		this.dateRetour = dateRetour;
		this.retour = retour;
		relanceHolder = new ArrayList<Relance>();
	}

	public Pret(Integer id) {
		this.id = id;
	}

	/**
	 * retourne l'état de sisponibilité du pret
	 * 
	 * @return retourne l'état du prêt sur son retour
	 */
	public boolean getDispo() {
		return getRetour();
	}

	/**
	 * Calcul le nombre de jour de retard et le retourne
	 * 
	 * @return Nombre de jour de retard
	 */
	public Integer estEnRetard() {
		if (dateRetour.isBefore(LocalDateTime.now())) {
			return null;
		}
		return toIntExact(dateRetour.until(LocalDateTime.now(), ChronoUnit.DAYS));
	}

	/**
	 * Getter de l'identifiant Pret
	 * 
	 * @return id Identifiant du Pret
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter de l'identifiant Pret
	 * 
	 * @param id
	 *            Identifiant du Pret
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter de la date de sortie
	 * 
	 * @return dateSortie Date de sortie du Pret
	 */
	public LocalDateTime getDateSortie() {
		return dateSortie;
	}

	/**
	 * Setter de la date de sortie
	 * 
	 * @param dateSortie
	 *            Date de sortie du Pret
	 */
	public void setDateSortie(LocalDateTime dateSortie) {
		this.dateSortie = dateSortie;
	}

	/**
	 * Getter de la date de retour
	 * 
	 * @return dateRetour Date de retour du Pret
	 */
	public LocalDateTime getDateRetour() {
		return dateRetour;
	}

	/**
	 * Setter de la date de sortie
	 * 
	 * @param dateRetour
	 *            Date de retour du Pret
	 */
	public void setDateRetour(LocalDateTime dateRetour) {
		this.dateRetour = dateRetour;
	}

	/**
	 * Getter pour savoir l'état du Pret sur son retour.
	 * 
	 * @return retour Si il a bien était retourné
	 */
	public boolean getRetour() {
		return retour;
	}

	/** Setter de l'état de retour du pret */

	/**
	 * Setter pour définir l'état du Pret sur son retour.
	 * 
	 * @param retour
	 *            Si il a bien était retourné
	 */
	public void setRetour(boolean retour) {
		this.retour = retour;
	}

	/**
	 * Getter de l'instance Ouvrage associé au Pret
	 * 
	 * @return Ouvrage Instance de Ouvrage
	 * @see Ouvrage
	 */
	public Ouvrage getOuvrage() {
		return Ouvrage;
	}

	/**
	 * Setter du Ouvrage associé au Pret
	 * 
	 * @param Ouvrage
	 *            Instance de Ouvrage
	 * @see Ouvrage
	 */
	public void setOuvrage(Ouvrage Ouvrage) {
		this.Ouvrage = Ouvrage;
	}

	/**
	 * Permet de retourner la liste de relances du Pret
	 * 
	 * @return relanceHolder
	 * @see Relance
	 */
	public ArrayList<Relance> getRelanceHolder() {
		return relanceHolder;
	}

	/**
	 * Permet de mettre à jour la liste de relances du prêt
	 * 
	 * @param relanceHolder
	 *            ArrayList contenant tout les relance associé à une instance
	 *            Pret
	 * @see Relance
	 */
	public void setRelanceHolder(ArrayList<Relance> relanceHolder) {
		this.relanceHolder = relanceHolder;
	}

	/**
	 * Getter de l'Usager associé à ce Pret
	 * 
	 * @return usager Usager associé
	 */
	public Usager getUsager() {
		return usager;
	}

	/**
	 * Setter de l'Usager associé
	 * 
	 * @param usager
	 *            Usager associé
	 */
	public void setUsager(Usager usager) {
		this.usager = usager;
	}

}

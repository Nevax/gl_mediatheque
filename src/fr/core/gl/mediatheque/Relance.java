package fr.core.gl.mediatheque;

import java.time.LocalDateTime;

/**
 * Class implémentant les Relance réalisé
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class Relance {

	/** Identifiant de la relance */
	private Integer			id;
	/** Date de la relance */
	private LocalDateTime	date;
	/** Libelle du courrier */
	private String			libCourrier;
	/** Etat de la relance */
	private Integer			codeEtat;
	/** Mode de l'envoi de la relance */
	private String			codeMode;
	/** Pret associé */
	private Pret			pret;

	/**
	 * Constructeur de Relance
	 * 
	 * @param id
	 *            Identifiant de la Relance
	 * @param pret
	 *            Pret associé à cette Relance
	 * @param date
	 *            Date de la Relance
	 * @param libCourrier
	 *            Libelle courrier de la Relance
	 * @param codeEtat
	 *            Etat de la relance
	 * @param codeMode
	 *            Mode d'envoi de la Relance
	 */
	public Relance(Integer id, Pret pret, LocalDateTime date, String libCourrier, Integer codeEtat, String codeMode) {
		this.id = id;
		this.pret = pret;
		this.date = date;
		this.libCourrier = libCourrier;
		this.codeEtat = codeEtat;
		this.codeMode = codeMode;
	}

	public Relance(Integer id) {
		this.id = id;
	}

	/**
	 * Getter du Pret associé à la relance
	 * 
	 * @return pret Instance de Pret
	 * @see Pret
	 */
	public Pret getPret() {
		return pret;
	}

	/**
	 * Setter du Pret associé à la relance
	 * 
	 * @param pret
	 *            Instance de Pret
	 * @see Pret
	 */
	public void setPret(Pret pret) {
		this.pret = pret;
	}

	/**
	 * Getter de l'identifiant de la Relance
	 * 
	 * @return id Identifiant de la relance
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter de l'identifiant de la Relance
	 * 
	 * @param id
	 *            Identifiant de la relance
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter de la date de la Relance
	 * 
	 * @return date Date de la Relance
	 */
	public LocalDateTime getDate() {
		return date;
	}

	/**
	 * Setter de la date de la Relance
	 * 
	 * @param date
	 *            Date de la Relance
	 */
	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	/**
	 * Getter du libelle du courrier de la Relance
	 * 
	 * @return libCourrier Libelle du courrier
	 */
	public String getLibCourrier() {
		return libCourrier;
	}

	/**
	 * Setter du libelle du courrier de la Relance
	 * 
	 * @param libCourrier
	 *            Libelle du courrier
	 */
	public void setLibCourrier(String libCourrier) {
		this.libCourrier = libCourrier;
	}

	/**
	 * Getter du code etat de la Relance
	 * 
	 * @return codeEtat
	 */
	public Integer getCodeEtat() {
		return codeEtat;
	}

	/**
	 * Setter du code etat
	 * 
	 * @param codeEtat
	 *            Etat de la relance sous forme de int
	 */
	public void setCodeEtat(int codeEtat) {
		this.codeEtat = codeEtat;
	}

	/**
	 * Getter du mode de l'envoi de la relance
	 * 
	 * @return codeMode
	 */
	public String getCodeMode() {
		return codeMode;
	}

	/**
	 * Setter du mode de l'envoi de la relance
	 * 
	 * @param codeMode
	 *            Mode de l'envoi de la relance
	 */
	public void setCodeMode(String codeMode) {
		this.codeMode = codeMode;
	}

	/**
	 * Permet de changer le codeEtat de la relance avec celui de l'envoi.
	 */
	public void envoyer() {
		setCodeEtat(1);
	}

	/**
	 * Permet de changer le codeEtat de la relance pour permettre de différer.
	 */
	public void differer() {
		setCodeEtat(2);
	}

	/**
	 * Permet de changer le codeEtat de la relance pour abandonner la relance.
	 */
	public void abandonner() {
		setCodeEtat(3);
		pret.getOuvrage().setDisponible(false);
		pret.getOuvrage().setPretable(false);
	}

	/**
	 * Permet de changer le codeEtat de la relance avec celui de la restitution.
	 */
	public void restituer() {
		setCodeEtat(4);
		pret.getOuvrage().setPretable(true);
		pret.getOuvrage().setDisponible(true);
	}
}

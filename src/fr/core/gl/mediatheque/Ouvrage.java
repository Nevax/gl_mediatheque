package fr.core.gl.mediatheque;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Class permettant l'instanciation de Ouvrage.
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class Ouvrage {

	/** Code du Ouvrage */
	private Integer			code;
	/** Titre du Ouvrage */
	private String			titre;
	/** Si le Ouvrage est prétable */
	private boolean			pretable;
	/** Si le Ouvrage est disponnible */
	private boolean			disponible;
	/** La date d'achat du Ouvrage */
	private LocalDateTime	dateAchat;

	/**
	 * Auteur relié au Ouvrage
	 * 
	 * @see Auteur
	 */
	private Auteur			auteur;
	/**
	 * Editeur du Ouvrage
	 * 
	 * @see Editeur
	 */
	private Editeur			editeur;
	/** Liste des prêts touchant ce Ouvrage 
	 * @see Pret
	 */
	private ArrayList<Pret>	pretHolder;

	/**
	 * Constructeur de la Class Ouvrage
	 * 
	 * @param code
	 *            Code du Ouvrage
	 * @param titre
	 *            Titre du Ouvrage
	 * @param pretable
	 *            Si le Ouvrage est prétable ou non
	 * @param disponible
	 *            Si le Ouvrage est disponnible ou non
	 * @param dateAchat
	 *            Date d'achat du docment
	 * @param editeur
	 *            Instance Editeur du Ouvrage
	 * @param auteur
	 *            Instance Auteur du Ouvrage
	 * @see Editeur
	 * @see Auteur
	 */
	public Ouvrage(Integer code, String titre, Boolean pretable, Boolean disponible, LocalDateTime dateAchat,
			Editeur editeur, Auteur auteur) {
		this.code = code;
		this.titre = titre;
		this.pretable = pretable;
		this.disponible = disponible;
		this.dateAchat = dateAchat;
		this.dateAchat = dateAchat;
		this.editeur = editeur;
		this.auteur = auteur;
		pretHolder = new ArrayList<Pret>();
	}

	/**
	 * Constructeur de la classe Ouvrage, appelant un autre constructeur
	 */
//	public Ouvrage() {
//		this(0, "Unknown", false, false, LocalDateTime.now(), null, null);
//	}

//	public Ouvrage(Integer id, String titre, Boolean pretable, Boolean disponible, Editeur editeur) {
//		this.code = id;
//		this.titre = titre;
//		this.pretable = pretable;
//		this.disponible = disponible;
//		this.editeur = editeur;
//	}

	/**
	 * Constructeur
	 * @param id Identifiant
	 */
	public Ouvrage(int id) {
		this.code = id;
	}

	/**
	 * Getter de l'ArrayList composé des instances Pret relié à ce Docuement
	 * 
	 * @return pretHolder ArrayList contenant les les Pret associé à ce Ouvrage
	 * @see ArrayList
	 * @see Pret
	 */
	public ArrayList<Pret> getPretHolder() {
		return pretHolder;
	}

	/**
	 * Setter de l'ArrayList composé des instances Pret relié à ce Docuement
	 * 
	 * @param pretHolder
	 *            ArrayList contenant les Pret associé au Ouvrage
	 * @see Pret
	 */
	public void setPretHolder(ArrayList<Pret> pretHolder) {
		this.pretHolder = pretHolder;
	}

	/**
	 * Setter de la prétabilité du Ouvrage
	 * 
	 * @param pretable
	 *            Etat de la prétabilité
	 */
	public void setPretable(Boolean pretable) {
		this.pretable = pretable;
	}

	/**
	 * Getter de la prétabilité du Ouvrage
	 * 
	 * @return pretable Etat de la prétabilité
	 */
	public boolean getPretable() {
		return pretable;
	}

	/**
	 * Setter de la disponibilité du Ouvrage
	 * 
	 * @param disponible
	 *            Etat de la disponibilité
	 */
	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}

	/**
	 * Getter de la disponibilité du Ouvrage
	 * 
	 * @return disponible Etat de disponnibilité du Docuement
	 */
	public boolean getDisponible() {
		return disponible;
	}

	/**
	 * Getter code identifiant le Ouvrage
	 * 
	 * @return code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Setter du code identifiant le Docuement
	 * 
	 * @param code
	 *            Code identifiant le
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Getter du titre du Ouvrage
	 * 
	 * @return titre Titre du Ouvrage
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Setter tu titre du Docuement
	 * 
	 * @param titre
	 *            Titre du Ouvrage
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * Getter de la date d'achat du Ouvrage
	 * 
	 * @return Date d'achat du Ouvrage
	 */
	public LocalDateTime getDateAchat() {
		return dateAchat;
	}

	/**
	 * Setter de la date d'achat du Ouvrage
	 * 
	 * @param dateAchat
	 *            Dated'achat du Ouvrage
	 */
	public void setDateAchat(LocalDateTime dateAchat) {
		this.dateAchat = dateAchat;
	}

	/**
	 * Getter de l'instance Auteur du Ouvrage
	 * 
	 * @return auteur Instance Auteur du Ouvrage
	 */
	public Auteur getAuteur() {
		return auteur;
	}

	/**
	 * Setter de l'instance Auteur du Ouvrage
	 * 
	 * @param auteur
	 *            Instance Auteur du Ouvrage
	 */
	public void setAuteur(Auteur auteur) {
		this.auteur = auteur;
	}

	/**
	 * Getter de l'instance Editeur du Ouvrage
	 * 
	 * @return editeur Instance Editeur du Ouvrage
	 */
	public Editeur getEditeur() {
		return editeur;
	}

	/**
	 * Setter de l'instance Editeur du Ouvrage
	 * 
	 * @param editeur
	 *            Instance Editeur du Ouvrage
	 */
	public void setEditeur(Editeur editeur) {
		this.editeur = editeur;
	}

	public String getPretableToString() {
		if (pretable) {
			return "Oui";
		} else {
			return "Non";
		}
	}
	
	public String getDisponibleToString() {
		if (disponible) {
			return "Oui";
		} else {
			return "Non";
		}
	}

}

package fr.core.gl.mediatheque;

import java.util.ArrayList;

import fr.dao.gl.mediatheque.AbstractDAOFactory;
import fr.dao.gl.mediatheque.DAO;
import fr.dao.gl.mediatheque.DAOSettings;
import fr.dao.gl.mediatheque.FactoryType;

/**
 * Super Class Mediatheque
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class Mediatheque {
	
	private static Usager		usager;
	private static Ouvrage		ouvrage;
	private static Pret			pret;

	/**
	 * DAO d'Usager en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Usager
	 */
	protected static DAO<Usager>	usagerdao;
	/**
	 * DAO de Ouvrage en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Ouvrage
	 */
	protected static DAO<Ouvrage>	ouvragedao;
	/**
	 * DAO de l'Auteur en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Auteur
	 */
	protected static DAO<Auteur>	auteurdao;
	/**
	 * DAO de Pret en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Pret
	 */
	protected static DAO<Pret>	pretdao;
	/**
	 * DAO de Relance en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Relance
	 */
	protected static DAO<Relance>	relancedao;
	/**
	 * DAO de l'Editeur en fonction du type de DAO indiqué dans le fichier dee
	 * configuration
	 * 
	 * @see AbstractDAOFactory
	 * @see DAOSettings
	 * @see Editeur
	 */
	protected static DAO<Editeur>	editeurdao;


	/** ArrayList contenant tout les Auteur de la Mediatheque */
	protected static ArrayList<Auteur>	auteurHolderAll		= new ArrayList<Auteur>();
	/** ArrayList contenant tout les Pret de la Mediatheque */
	protected static ArrayList<Pret>	pretHolderAll		= new ArrayList<Pret>();
	/** ArrayList contenant tout les Relance de la Mediatheque */
	protected static ArrayList<Relance>	relanceHolderAll	= new ArrayList<Relance>();
	/** ArrayList contenant tout les Ouvrage de la Mediatheque */
	protected static ArrayList<Ouvrage>	ouvrageHolderAll	= new ArrayList<Ouvrage>();
	/** ArrayList contenant tout les Eediteur de la Mediatheque */
	protected static ArrayList<Editeur>	editeurHolderAll	= new ArrayList<Editeur>();
	/** ArrayList contenant tout les Usager de la Mediatheque */
	protected static ArrayList<Usager>	usagerHolderAll		= new ArrayList<Usager>();

	public static ArrayList<Auteur> getAuteurHolderAll() {
		return auteurHolderAll;
	}

	public void setAuteurHolderAll(ArrayList<Auteur> auteurHolderAll) {
		Mediatheque.auteurHolderAll = auteurHolderAll;
	}

	public static ArrayList<Pret> getPretHolderAll() {
		return pretHolderAll;
	}

	public void setPretHolderAll(ArrayList<Pret> pretHolderAll) {
		Mediatheque.pretHolderAll = pretHolderAll;
	}

	public static ArrayList<Relance> getRelanceHolderAll() {
		return relanceHolderAll;
	}

	public void setRelanceHolderAll(ArrayList<Relance> relanceHolderAll) {
		Mediatheque.relanceHolderAll = relanceHolderAll;
	}

	public static ArrayList<Ouvrage> getOuvrageHolderAll() {
		return ouvrageHolderAll;
	}

	public void setOuvrageHolderAll(ArrayList<Ouvrage> OuvrageHolderAll) {
		Mediatheque.ouvrageHolderAll = OuvrageHolderAll;
	}

	public static ArrayList<Editeur> getEditeurHolderAll() {
		return editeurHolderAll;
	}

	public void setEditeurHolderAll(ArrayList<Editeur> editeurHolderAll) {
		Mediatheque.editeurHolderAll = editeurHolderAll;
	}

	public static ArrayList<Usager> getUsagerHolderAll() {
		return usagerHolderAll;
	}

	public void setUsagerHolderAll(ArrayList<Usager> usagerHolderAll) {
		Mediatheque.usagerHolderAll = usagerHolderAll;
	}

}

package fr.core.gl.mediatheque;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class implémentant les Usager inscrit auprès de la mediatheque
 * 
 * @author Antoine GRAVELOT
 * @version 2.0
 */
public class Usager {

	/** Identifiant de l'Usager */
	private Integer				id;
	/** Civilité de l'usager */
	private Integer				civilite;
	/** Nom de l'usager */
	private String				nom;
	/** Prenom de l'usager */
	private String				prenom;
	/** Adresse postale de l'usager */
	private String				adressePostale;
	/** Date de l'instription de l'Usager */
	private LocalDateTime		dateInscription;
	/** Contenant le montant de la cotisation */
	private float				cotisation;
	/** Le nombre de pret de l'usager */
	private Integer				nombrePret;
	/** Contenant l'adresse email de l'usager */
	private String				adresseEmail;

	/**
	 * Collection contenant les prets
	 */
	public ArrayList<Pret>		pretHolder;
	/**
	 * Collection contenant les relances
	 */
	public ArrayList<Relance>	relanceHolder;

	DateFormat					dateFormat	= new SimpleDateFormat("dd/MM/yyyy");
	Date						date		= new Date();

	/**
	 * Constructeur Usager, initialisant les attribut de l'objet Usager avec les
	 * valeurs "Unknown".
	 */
	public Usager() {
		this(0, -1, "Unknown", "Unknown", "Unknown", "Unknown");
	}

	/**
	 * Constructeur d'Usager
	 * 
	 * @param id
	 *            Identifiant Usager
	 * @param civilite
	 *            Civilité Usager
	 * @param nom
	 *            Nom Usager
	 * @param prenom
	 *            Prenom Usager
	 * @param adressePostale
	 *            Adresse postale Usager
	 * @param adresseEmail
	 *            Adresse email Usager
	 */
	public Usager(Integer id, Integer civilite, String nom, String prenom, String adressePostale, String adresseEmail) {
		// TODO Auto generate id
		this.id = id;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.adressePostale = adressePostale;
		this.adresseEmail = adresseEmail;
		this.dateInscription = LocalDateTime.now();
		pretHolder = new ArrayList<Pret>();
		relanceHolder = new ArrayList<Relance>();
	}

	/**
	 * 
	 * @param id
	 *            Identifiant Usager
	 * @param civilite
	 *            Civilité Usager
	 * @param nom
	 *            Nom Usager
	 * @param prenom
	 *            Prenom Usager
	 * @param adressePostale
	 *            Adresse postale Usager
	 * @param dateInscription
	 *            Date d'inscription
	 * @param cotisation
	 *            Montant de sa cotisation
	 * @param nombrePret
	 *            Nombre de Pret réalisé par l'Usager
	 * @param adresseEmail
	 *            Adresse email Usager
	 */
	public Usager(Integer id, Integer civilite, String nom, String prenom, String adressePostale, Date dateInscription,
			float cotisation, Integer nombrePret, String adresseEmail) {
		this.id = id;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.adressePostale = adressePostale;
		this.cotisation = cotisation;
		this.nombrePret = nombrePret;
		this.adresseEmail = adresseEmail;
		// TODO Fix Localdate / date
		this.dateInscription = LocalDateTime.now();
	}

	/**
	 * Constructeur
	 * @param id Identifiant
	 */
	public Usager(int id) {
		this.id = id;
	}

	/**
	 * WIP Recherche l'usager portant l'id donné en paramètre dans l'ArrayList
	 * donné en paramètre
	 * 
	 * @param arrayListUsager
	 *            ArrayList a fouiller
	 * @param id
	 *            Identifiant de l'usager
	 * @return Retourne l'index de l'usager portant l'id
	 */
	public static int findUsager(ArrayList<Usager> arrayListUsager, Integer id) {
		return arrayListUsager.indexOf(id);
	}

	/**
	 * Getter de id
	 * 
	 * @return id identifiant usagé
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Setter de id
	 * 
	 * @param id identifiant usagé
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Ajouter un Pret à cet Usager
	 * 
	 * @param pret
	 *            Le Pret
	 */
	public void ajouterPret(Pret pret) {
		pretHolder.add(pret);
	}

	/**
	 * Définie le montant de la cotisation
	 * 
	 * @param f
	 *            Montant de la cotisation
	 */
	public void reglerCotisation(float f) {
		setCotisation(f);
	}

	/**
	 * Retourne la liste des prêts réalisé par de cet Usager sous forme
	 * d'ArrayList contenant les instances de différentes Pret.
	 * 
	 * @return pretHolder Liste des Pret
	 */
	public ArrayList<Pret> listerPret() {
		return pretHolder;
	}

	/**
	 * Retourne la liste des relances envoyé auprès de cet Usager sous forme
	 * d'ArrayList contenant les instances de différentes relances.
	 * 
	 * @return relanceHolder Liste des Relance
	 */
	public ArrayList<Relance> listerHistoriqueRelance() {
		return relanceHolder;
	}

	/**
	 * Getter de la civilité de l'Usager
	 * 
	 * @return civilite La civilité sous forme d'Integer
	 */
	public Integer getCivilite() {
		return civilite;
	}
	
	public String getCiviliteToString() {
		if (civilite == 1) {
			return "Homme";
		} else if (civilite == 0) {
			return "Femme";
		} else {
			return "Inconnu";
		}
	}

	/**
	 * Setter de la civilité de l'Usager
	 * 
	 * @param civilite
	 *            La civilité sous forme d'Integer
	 */
	public void setCivilite(Integer civilite) {
		this.civilite = civilite;
	}

	/**
	 * Getter du nom de l'Usager
	 * 
	 * @return nom de l'usager
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Stter du nom de l'Usager
	 * 
	 * @param nom
	 *            de l'usager
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Getter du prénom de l'Usager
	 * 
	 * @return prenom de l'usager
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Setter du prenom de l'Usager
	 * 
	 * @param prenom
	 *            Prénom dee l'usager
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Getter de l'aderesse postale de l'Usager.
	 * 
	 * @return adressePostale aderesse postale de l'Usager.
	 */
	public String getAdressePostale() {
		return adressePostale;
	}

	/**
	 * Setter de l'aderesse postale de l'Usager.
	 * 
	 * @param adressePostale
	 *            aderesse postale de l'Usager.
	 */
	public void setAdressePostale(String adressePostale) {
		this.adressePostale = adressePostale;
	}

	/**
	 * Getter de la date d'inscription de l'Usager
	 * 
	 * @return dateInscription date de l'inscription
	 */
	public LocalDateTime getDateInscription() {
		return dateInscription;
	}

	/**
	 * Getter du montant de la cotisation par l'Usager.
	 * 
	 * @return cotisation Montant de la cotisation
	 */
	public float getCotisation() {
		return cotisation;
	}

	/**
	 * Setter du montant de la cotisation par l'Usager.
	 * 
	 * @param cotisation
	 *            Montant de la cotisation
	 */
	public void setCotisation(float cotisation) {
		this.cotisation = cotisation;
	}

	/**
	 * Getter du nombre de Pret réalisé par l'Usager
	 * 
	 * @return nombrePret Nombre de prêts
	 */
	public int getNombrePret() {
		return nombrePret;
	}

	/**
	 * Setter du nombre de Pret réalisé par l'Usager
	 * 
	 * @param nombrePret
	 *            Nombre de prêts
	 */
	public void setNombrePret(Integer nombrePret) {
		this.nombrePret = nombrePret;
	}

	/**
	 * Getter de l'adresse Mail de l'usager
	 * 
	 * @return adresseEmail Adresse Mail
	 */
	public String getAdresseEmail() {
		return adresseEmail;
	}

	/**
	 * Setter de l'adresse Mail de l'usager
	 * 
	 * @param adresseEmail
	 *            AdresseMail
	 */
	public void setAdresseEmail(String adresseEmail) {
		this.adresseEmail = adresseEmail;
	}
}
